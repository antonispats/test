<?php
    // Requirements
    require 'configCloud.php';
	require 'settingsCloud.php';
    require 'config.php';
    include 'includes/functions.php';
    
    if(isset($_POST['eventsNewCount']) && isset($_POST['name'])) {
        $eventsNewCount = $_POST['eventsNewCount'];
        $user = $_POST['name'];
        $eventCount = $eventsNewCount + 2;
        
        // fetch all Events
        $stmt = $conn->prepare
        (
            "SELECT `id`, `image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `start_time`, `end_time`, `description`, `tickets`
            FROM `events`, `org_events`
            WHERE `org_events`.`org_name` = ? AND `org_events`.`event` = `name` LIMIT $eventsNewCount,$eventCount"
        );
        mysqli_stmt_bind_param($stmt, 's', $user);
        mysqli_stmt_execute($stmt);

        $events = array();

        $result = mysqli_stmt_get_result($stmt);

        if(mysqli_num_rows($result) > 0) {
            while ($data = mysqli_fetch_assoc($result)) {
                $events[] = $data;
            }
        }
    }
    $index = $eventsNewCount;
    foreach ($events as $event) {
        ?>
        <div class="card event" style="width: auto;">
            <?php $link = "../events/".$event["id"]; ?>
            
            <a href="<?php echo $link ?>">
                <?php
                    $img = array(
                        'public_id' => $event["image"],
                    );
                    echo cl_image_tag(
                        $img['public_id'],
                        array("format" => "jpg", "crop" => "fill")
                    );
                ?>
            </a>
            <div class="card-body">
                <p class="card-title" style="color:red;">
                    <?php
                    $start_date_formatted = date('d F Y', strtotime($event["start_date"]));
                    $start_time_formatted = date('G:i A', strtotime($event["start_time"]));

                    echo $start_date_formatted . " at " . $start_time_formatted;
                    ?>
                </p>
                <h5 class="card-title"><?php echo htmlspecialchars($event['name'], ENT_QUOTES, 'UTF-8'); ?></h5>
                <p class="card-title" style="color:grey;">
                    <?php echo "<i class='fas fa-map-marker-alt'></i>&nbsp;&nbsp;" . htmlspecialchars(ucfirst($event["location"]), ENT_QUOTES, 'UTF-8') ;?>
                </p>
                <?php
                ?>
                <hr>
                <p class="card-text">
                    <div id="description-common" class="description-common">
                        <?php
                            if (strlen($event["description"]) > 68) {
                                echo htmlspecialchars(substr($event["description"], 0, 65), ENT_QUOTES, 'UTF-8');
                                echo "<span id='event-dots' class='event-dots-span'>...</span>";
                                echo "<span id='event-moretext' class='event-moretext-span'>" . htmlspecialchars(substr($event["description"], 65, strlen($event["description"])), ENT_QUOTES, 'UTF-8') . "</span>";
                                echo "<br />";
                                echo "<small><a onclick='showMoreEventInfo($index)' id='rm-event' class='show-more'>Read More</a></small>";
                            } else {
                                echo htmlspecialchars($event["description"], ENT_QUOTES, 'UTF-8');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <?php $index++;
    }
?>
<script>
    // Reload profile.js
    function reload_js(src) {
            $('script[src="' + src + '"]').remove();
            $('<script>').attr('src', src).appendTo('body');
    }
    reload_js('../assets/js/profile.js')   ;
</script>