<footer class="footer">
	<text>University Opportunities Ⓒ 2019 • </text>
	<a href="../about-us" id="about-us-a" style="color: #66615B;">About Us</a>
	<text> • </text>
	<i class="fab fa-facebook"></i>
	<a href="https://www.facebook.com/UniversityOpportunities" id="facebook-link" target="_blank" style="color: #66615B;">Facebook</a>
	<text> • </text>
	<i class="fab fa-instagram"></i>
	<a href="https://www.instagram.com/uni.ties" id="instagram-link" target="_blank" style="color: #66615B;">Instagram</a>
	<text> • </text>
	<i class="fab fa-linkedin-in"></i>
	<a href="https://www.linkedin.com/company/unities" id="instagram-link" target="_blank" style="color: #66615B;">LinkedIn</a>
	<text> • </text>
	<a href="../bug-report" id="report-problem-a" style="color: #66615B;">Report a problem</a>
</footer>
