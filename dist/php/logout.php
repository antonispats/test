<?php
	session_start();
	session_unset();
	session_destroy();

	if (isset($_COOKIE['user'])) {
		include_once 'includes/global-data.php';
		
		setcookie('user', '', time() - $EXPIRE_AFTER, '/');
	}
	
	header('Location: https://uni-ties.gr');
?>