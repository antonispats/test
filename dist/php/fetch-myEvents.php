<?php
    // Requirements
    require 'configCloud.php';
	require 'settingsCloud.php';
    require 'config.php';
    include 'includes/functions.php';
    
    if(isset($_POST['eventsNewCount']) && isset($_POST['name'])) {
        $eventsNewCount = $_POST['eventsNewCount'];
        $user = $_POST['name'];
        $eventCount = $eventsNewCount + 8;
        
        // fetch all Events
        $stmt = $conn->prepare
        (
            "SELECT `id`, `image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `start_time`, `end_time`, `description`, `tickets`
            FROM `events`, `org_events`
            WHERE `org_events`.`org_name` = ? AND `org_events`.`event` = `name` LIMIT $eventsNewCount,$eventCount"
        );
        mysqli_stmt_bind_param($stmt, 's', $user);
        mysqli_stmt_execute($stmt);

        $events = array();

        $result = mysqli_stmt_get_result($stmt);

        if(mysqli_num_rows($result) > 0) {
            while ($data = mysqli_fetch_assoc($result)) {
                $events[] = $data;
            }
        }
    }
    
    $index = 1;
    $last = 0;
    foreach ($events as $event) {
        if ($index - $last == 1) echo "<div class='row'>";
        ?>
        <!-- Events Card -->
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card event" style="width: auto;">
                <?php $link = "../events/".$event["id"]; ?>

                <a href="<?php echo $link ?>">
                    <?php
                        $img = array(
                            'public_id' => $event["image"],
                        );
                        echo cl_image_tag(
                            $img['public_id'],
                            array("format" => "jpg", "width" => "448", "height" => "200", "crop" => "scale")
                        );
                    ?>
                </a>
                <div class="card-body">
                    <p class="card-title" style="color:red;">
                        <?php
                        $start_date_formatted = date('d F Y', strtotime($event["start_date"]));
                        $start_time_formatted = date('g:i A', strtotime($event["start_time"]));

                        echo $start_date_formatted . " at " . $start_time_formatted;
                        ?>
                    </p>
                    <h5 class="card-title"><?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8'); ?></h5>
                    <p class="card-title" style="color: grey;"><?php echo htmlspecialchars($event["location"], ENT_QUOTES, 'UTF-8'); ?></p>
                    <?php $link = "../edit-event/".$event["id"] ?>
					<a href="<?php echo $link; ?>" class="btn btn-info" id="settings-button">Edit</a>
                    <button onclick="openModal(<?php echo $index ?>)" type="button" class="btn btn-danger" id="cancel-button">Delete
                </div>
            </div>
        </div>

        <!-- Delete Modal Window -->
        <div class="modal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Delete Confirmation</h5>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>
                    <div class="modal-body">
                        <b>Warning: </b>'<?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8'); ?>' will be permanently deleted.<br>Are you sure you want to proceed?
                    </div>
                    <div class="modal-footer">
                        <button onclick="location.href='my-events.php?del=<?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8') ?>'" type="submit" class="btn btn-danger" id="cancel-button">Delete</button>
                        <button onclick="closeModal(<?php echo $index ?>)" type="button" class="btn btn-primary" data-dismiss="modal" id="settings-button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($index % 4 == 0) {
            echo "</div>";
            $last = $index;
        }
        $index++;
    }