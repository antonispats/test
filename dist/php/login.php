<?php
    include 'includes/functions.php';
    include 'includes/error-messages.php';

    // Get form data
    if ($_SERVER["REQUEST_METHOD"] == "POST") { 
        $email = $_POST['email'];
        $password = $_POST['password'];
        $remember = isset($_POST['remember']);

        // Check Email
        if (!is_valid_email($email)) {
            head("../index.php?error=ief");
        }
        else {
            if ($row = user_exists($email)) {
                $name = $row['name'];
                $hash = $row['password'];
                $active = $row['active'];

                if ($active == 1) {
                    // if user is verified

                    // password verification
                    if (password_verify($password, $hash)) {
                        login_user($name, $remember);
                    } else {
                        // if password is wrong
                        head("../index.php?error=iep");
                    }
                } else {
                    // if user is not verified
                    head("../index.php?error=uu");
                }
                
            } else {
                head("../index.php?error=ude");
            }
        }
        exit();
    }
?>