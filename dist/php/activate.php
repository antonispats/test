<?php
	session_start();

	include 'config.php';

	if ($_SERVER['REQUEST_METHOD'] == "GET") {
		if (isset($_GET['email'])) {
			$email = $_GET['email'];
			$token = $_GET['token'];

			$stmt = $conn->prepare
    		(
    			"SELECT `name` FROM `accounts` WHERE `email`=? AND `validation_code`=?"
    		);

			mysqli_stmt_bind_param($stmt, 'ss', $email, $token);
    		mysqli_stmt_execute($stmt);

    		$result = mysqli_stmt_get_result($stmt);

			// if any user found
			if (mysqli_num_rows($result) == 1) {
				$query = "UPDATE accounts SET active = 1, validation_code = '0' WHERE email=? AND validation_code=?";
				$updateStmt = mysqli_stmt_init($conn);

				if (!mysqli_stmt_prepare($updateStmt, $query)) {
		            head("activate.php?error=sqlerror");
		        } else {
		        	mysqli_stmt_bind_param($updateStmt, 'ss', $email, $token);
		    		$success = mysqli_stmt_execute($updateStmt);

					if ($success) {
						// success
						$_SESSION['verificationMessage'] = "<p class='p-3 mb-2 bg-success text-white'>Your account has successfully been verified! You can log in now.</p>";
						header("Location: ../index.php");
						exit();
					} else {
						$_SESSION['verificationMessage'] = "<p class='p-3 mb-2 bg-danger text-white'>Your account could not be verified.</p>";
						header("Location: ../index.php");
						exit();
					}
		        }
			} else {
				$_SESSION['verificationMessage'] = "<p class='p-3 mb-2 bg-danger text-white'>Your account could not be verified.</p>";
				header("Location: ../index.php");
				exit();
			}
		} else {
			$_SESSION['verificationMessage'] = "<p class='p-3 mb-2 bg-danger text-white'>Your account could not be verified.</p>";
			header("Location: ../index.php");
			exit();
		}
	}
?>