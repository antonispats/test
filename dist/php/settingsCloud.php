<?php
if(file_exists('./envCloud.php')) {
    require './envCloud.php';
}

\Cloudinary::config(array(
    "cloud_name" => getenv('CLOUD_NAME'),
    "api_key" => getenv('API_KEY'),
    "api_secret" => getenv('API_SECRET')
));