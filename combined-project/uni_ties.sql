-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2019 at 10:58 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uni_ties`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `about` varchar(400) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`name`, `email`, `password`, `logo`, `about`, `category`) VALUES
('Aristomenis', 'aristos98@hotmail.com', '$2y$10$wt1ZEaidJ..9cA4tzHpD5uiEs7NSSnP8JfdaLOftLY7LAMrBoyNGK', 'events/logo-small', 'kanonikh perigrafh', 'MKO'),
('kati', 'kati@gmail.com', '$2y$10$DH5P3YMfFCoIuG8zyWMDFe6.E7uk59iUIQUcbkqOSqDKxRZ7OH7oy', 'events/default-avatar', '', 'MKO'),
('kostas', 'kos@gmail.com', '$2y$10$BezJB.nihONURftHUkdtZOPegegOn9KKAepbYk0poIeDh977reJ2a', 'logos/default-logo', '', ''),
('Sofiaa', 'sof63@gmail.com', '$2y$10$cq8dHnuvljbRhvismZOGKedff6HvJAs/DjDa5nQQNaiv.dK0zPMea', 'logos/dragon', '', 'MKO');

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `org_name` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` varchar(60) NOT NULL,
  `country` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_info`
--

INSERT INTO `contact_info` (`org_name`, `website`, `phone`, `address`, `country`) VALUES
('Aristomenis', 'www.unities.gr', '2105310588', '', ''),
('Sofiaa', 'www.sofia.gr', '2106969696', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `description` varchar(500) NOT NULL,
  `tickets` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `start_time`, `end_time`, `description`, `tickets`) VALUES
('events/damir-bosnjak', 'Angerfist Live', 'OAKA', 'Workshop', 'Business', '2019-08-22', '2019-09-06', '15:00:00', '23:00:00', 'Etoimasteite gia mia monadikh empeiria. xaxa', ''),
('events/jan-sendereks', 'Defqon.1 | 2019', 'Netherlands', 'Seminario', 'Marketing', '2019-08-13', '2019-08-26', '18:00:00', '21:00:00', 'KIOUMP', 'https://www.public.gr'),
('events/bg5', 'Party Spiti Aristomenh', 'Xytirio', 'Seminario', 'Marketing', '2019-08-29', '2019-09-08', '00:00:00', '00:00:00', 'touv', 'https://www.viva.gr'),
('events/mainstage', 'UNTOLD Festival', 'Romania', 'Diagonismos', 'Technology', '2019-08-15', '2019-08-23', '20:00:00', '11:00:00', 'Armin Live', 'https://www.viva.gr');

-- --------------------------------------------------------

--
-- Table structure for table `org_events`
--

CREATE TABLE `org_events` (
  `org_name` varchar(255) NOT NULL,
  `event` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_events`
--

INSERT INTO `org_events` (`org_name`, `event`) VALUES
('Aristomenis', 'Party Spiti Aristomenh'),
('Aristomenis', 'Defqon.1 | 2019'),
('Aristomenis', 'Angerfist Live'),
('Aristomenis', 'UNTOLD Festival');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`org_name`),
  ADD KEY `org_name_fk` (`org_name`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `org_events`
--
ALTER TABLE `org_events`
  ADD KEY `org_event_name_fk` (`org_name`),
  ADD KEY `event_name_fk` (`event`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD CONSTRAINT `org_name_fk` FOREIGN KEY (`org_name`) REFERENCES `accounts` (`name`) ON UPDATE CASCADE;

--
-- Constraints for table `org_events`
--
ALTER TABLE `org_events`
  ADD CONSTRAINT `event_name_fk` FOREIGN KEY (`event`) REFERENCES `events` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `org_event_name_fk` FOREIGN KEY (`org_name`) REFERENCES `accounts` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
