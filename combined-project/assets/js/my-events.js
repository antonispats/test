/*------------------------
    Delete Modal Window
-------------------------*/
// Get the modals
const modal = document.getElementsByClassName("modal");

// Get the <span> element that closes the modal
const span = document.getElementsByClassName("close")[0];

function openModal(i) {
    // console.log(modal[i-1]);
    modal[i-1].style.display = "block";
}

function closeModal(i) {
    modal[i-1].style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}