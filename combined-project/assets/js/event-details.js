function showMoreUserInfo() {
    var user_dots = document.getElementById("user-dots");
    var user_moretext = document.getElementById("user-moretext");
    var read_more_user = document.getElementById("rm-user");

    // show more or less info about user
    if (user_dots.style.display === "none") {
        user_dots.style.display = "inline";
        read_more_user.innerHTML = "Read more";
        user_moretext.style.display = "none";
    } else {
        user_dots.style.display = "none";
        read_more_user.innerHTML = "Read less";
        user_moretext.style.display = "inline";
    }
}

function showMoreEventInfo() {
    const event_dots = document.getElementById("event-dots");
    const event_moretext = document.getElementById("event-moretext");
    const event_showmore = document.getElementById("rm-event");

    // show more or less info about event
    if (event_dots.style.display === "none") {
        event_dots.style.display = "inline";
        event_moretext.style.display = "none";
        event_showmore.innerHTML = "Read more";
    } else {
        event_dots.style.display = "none";
        event_moretext.style.display = "inline";
        event_showmore.innerHTML = "Read less";
    }
}