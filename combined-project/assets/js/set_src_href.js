/*******	NAV BAR ELEMENTS		*****/
document.getElementById('index-a').href = '../index.php';
document.getElementById('search-form').action = '../index.php';
document.getElementById('filter-events-form').action = '../index.php';
/****************************************/

/*******	FOOTER AND CONTROL PANE ELEMENT		*****/
document.getElementById('report-problem-a').href = 'bug-report.php';
/****************************************************/


if (document.getElementById('image-logo') != null) {
	/*******	IF USER IS NOT LOGGED IN		*****/
	document.getElementById('image-logo').src = '../assets/img/unities_transparent.png';
	document.getElementById('login-form').action = 'login.php';
	/************************************************/
} else {
	/*******	CONTROL PANEL ELEMENTS		*****/
	document.getElementById('create-event-a').href = 'create-event.php';
	document.getElementById('my-events-a').href = 'my-events.php';
	document.getElementById('settings-a').href = 'settings.php';
	document.getElementById('communicate-a').href= 'communicate.php';
	document.getElementById('logout-a').href = 'logout.php';
	/********************************************/
}