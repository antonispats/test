function showMoreUserInfo() {
    var user_dots = document.getElementById("user-dots");
    var user_moretext = document.getElementById("user-moretext");
    var read_more_user = document.getElementById("rm-user");

    // show more or less info about user
    if (user_dots.style.display === "none") {
        user_dots.style.display = "inline";
        read_more_user.innerHTML = "Read more";
        user_moretext.style.display = "none";
    } else {
        user_dots.style.display = "none";
        read_more_user.innerHTML = "Read less";
        user_moretext.style.display = "inline";
    }
}

function showMoreEventInfo(i) {
    var all_descriptions = document.getElementsByClassName('description-common');
    var selectedDescription = document.querySelectorAll("div.description-common")[i];

    var index = 0;

    for (var y = 0; y < all_descriptions.length; y++) {
        var curr = all_descriptions[y];

        if (curr.childNodes.length > 1) {
            if (selectedDescription === curr) {
                break;
            } else {
                index++;
            }
        }
    }

    const event_dots = document.getElementsByClassName("event-dots-span");
    const event_moretext = document.getElementsByClassName("event-moretext-span");
    const event_showmore = document.getElementsByClassName("show-more");

    // show more or less info about event
    if (event_dots[index].style.display === "none") {
        event_dots[index].style.display = "inline";
        event_moretext[index].style.display = "none";
        event_showmore[index].innerHTML = "Read more";
    } else {
        event_dots[index].style.display = "none";
        event_moretext[index].style.display = "inline";
        event_showmore[index].innerHTML = "Read less";
    }
}