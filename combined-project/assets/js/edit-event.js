for (var i = 0; i < document.getElementById("category").options.length; i++) {
    if (document.getElementById("category").options[i].value == "<?php echo $category_old; ?>") {
        document.getElementById("category").options[i].selected = true;
    }
}

for (var i = 0; i < document.getElementById("type").options.length; i++) {
    if (document.getElementById("type").options[i].value == "<?php echo $type_old; ?>") {
        document.getElementById("type").options[i].selected = true;
    }
}



function checkFields() {
    var elements = document.querySelectorAll("input, textarea");

    for (var i = 0; i < elements.length; i++) {
        elements[i].removeAttribute("style");
    }

    for (var i = 0; i < elements.length; i++) {
        if (elements[i].hasAttribute('required') && elements[i].value === '') {
            elements[i].setAttribute("style", "border: 1px solid red;");
        }
    }
}