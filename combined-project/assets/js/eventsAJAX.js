/**
 * What does this AJAX do?
 * First of all we need the total number of events that exists in the database
 * So, we do a GET request to 'eventsNum.php' file and we get the total number of events (line 12)
 * When user scrolls to the bottom of the viewport (line 25)
 * if all events are fetched -> dont make an AJAX request 
 * else make an AJAX request and fetch some more events (line 29, 34)
 * Note: The global variable 'processing' is used to stop firing up multiple requests when scroll point is passed
 */

$(document).ready(function() {
    // Globals
    var processing;

    /* AJAX => Index Page */
    if ($("#index-page").length) {
        // AJAX => Get events number
        const content = document.querySelector('.content');
        let eventsNumber;
        let sPageURL = window.location.search.substring(1);
        // Get URL Parameters and make AJAX Call
        if (GetURLParameter('category')) {
            let category = GetURLParameter('category');
            $.ajax({
                type: 'get',
                url: `./php/eventsNum.php?category=${category}`,
                success: function(response) {
                    eventsNumber = parseInt(response);
                }
            });
        } else if (sPageURL === '') {
            $.ajax({
                type: 'get',
                url: './php/eventsNum.php',
                success: function(response) {
                    eventsNumber = parseInt(response);
                }
            });
        }
        // if (GetURLParameter('type')) {
        //     let type = GetURLParameter('type');
        //     console.log(type);
        // }
          
        // AJAX => Fetch events
        let eventsCount = 8;
        // if scroll position = bottom
        $(window).scroll(function() {
            if (processing) return false

            let scrollPercent = windowAmountScrolled();
            // When scroll reaches 80%
            if (scrollPercent >= 80) {
                processing = true;
                var eventsOnScreen = document.querySelectorAll('.event').length;

                if (eventsNumber > eventsOnScreen) {
                    // Display Loader
                    $("#lds-ring").show();

                    // Make a new row element
                    var newElement = document.createElement("div");
                    newElement.setAttribute('class', 'row')

                    $.ajax({
                        type: 'post',
                        url: './php/fetch-events.php',
                        data: { eventsNewCount: eventsCount },
                        success: function(response) {
                            if (response.length > 3) {
                                // Insert new events in the element and append it to content div
                                newElement.innerHTML = response;
                                content.appendChild(newElement);
                                eventsCount += 8;
                                processing = false;
                                $("#lds-ring").hide();
                            }
                        }
                    });
                }  
            }
        });
    }

    /* AJAX => Profile Page */
    if ($("#user-events").length) {
        // Get event organizer name
        const author = document.getElementById('author').innerHTML;
        const content = document.querySelector('#user-events');
        
        // AJAX => Get events number
        let eventsNumber;
        $.ajax({
            type: 'get',
            url: './eventsNum.php',
            data: { name: author },
            success: function(response) {
                eventsNumber = parseInt(response);
            }
        });
        
        // AJAX => Fetch events
        let eventsCount = 2;
        $("#user-events").scroll(function() {
            if (processing) return false;
            
            let scrollPercent = containerAmountScrolled();
            // When scroll reaches 80%
            if (scrollPercent >= 80) {
                processing = true;
                var eventsOnScreen = document.querySelectorAll('.event').length;

                if (eventsNumber > eventsOnScreen) { 
                    $.ajax({
                        type: 'post',
                        url: './fetch-events-by-name.php',
                        data: { eventsNewCount: eventsCount, name: author },
                        success: function(response) {
                            if (response.length > 3) {
                                // Insert new events in the element and append it to content div
                                let newContent = document.createRange().createContextualFragment(response);
                                content.appendChild(newContent);
                                eventsCount += 2;
                                processing = false;
                            }
                        }
                    });
                }  
            }
        })
    }

    /* AJAX => My Events Page */
    if ($("#my-events").length) {
        // Get event organizer name
        const author = document.getElementById('username').innerHTML;
        const content = document.querySelector('#my-events');
        
        // AJAX => Get events number
        let eventsNumber;
        $.ajax({
            type: 'get',
            url: './eventsNum.php',
            data: { name: author },
            success: function(response) {
                eventsNumber = parseInt(response);
            }
        });

        let eventsCount = 8;
        $("#my-events").scroll(function() {
            if (processing) return false;
            
            let scrollPercent = containerAmountScrolled();
            // When scroll reaches 80%
            if (scrollPercent >= 80) {
                processing = true;
                var eventsOnScreen = document.querySelectorAll('.event').length;

                if (eventsNumber > eventsOnScreen) {
                    // Display Loader
                    $("#lds-ring").show();

                    $.ajax({
                        type: 'post',
                        url: './fetch-myEvents.php',
                        data: { eventsNewCount: eventsCount, name: author },
                        success: function(response) {
                            console.log('AJAX');
                            if (response.length > 3) {
                                // Insert new events in the element and append it to content div
                                let newContent = document.createRange().createContextualFragment(response);
                                content.appendChild(newContent);
                                eventsCount += 8;
                                processing = false;
                                $("#lds-ring").hide();
                            }
                        }
                    });
                }  
            }
        })
    }

    // Functions
    function containerAmountScrolled(){
        var scrollPercent = 100 * $("#user-events").scrollTop() / ($("#user-events")[0].scrollHeight - $("#user-events").innerHeight());
        return scrollPercent;
    }
    function windowAmountScrolled() {
        var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
        return scrollPercent;
    }
    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
})