<?php

require '../vendor/autoload.php';
use MatthiasMullie\Minify;

/*~~~~~~~~~~CSS~~~~~~~~~~*/
/* paper-dashboard.css */
$sourcePath = './assets/css/paper-dashboard.css';
$minifier = new Minify\CSS($sourcePath);

$minifiedPath = '../dist/assets/css/paper-dashboard.css';
$minifier->minify($minifiedPath);

/*~~~~~~~~~~JavaScript~~~~~~~~~~*/
/* bootstrap-notify.js */
$sourcePath = './assets/js/plugins/bootstrap-notify.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/plugins/bootstrap-notify.js';
$minifier->minify($minifiedPath);

/* bug-report.js */
$sourcePath = './assets/js/bug-report.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/bug-report.js';
$minifier->minify($minifiedPath);

/* eventsAJAX.js */
$sourcePath = './assets/js/eventsAJAX.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/eventsAJAX.js';
$minifier->minify($minifiedPath);

/* create-event.js */
$sourcePath = './assets/js/create-event.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/create-event.js';
$minifier->minify($minifiedPath);

/* edit-event.js */
$sourcePath = './assets/js/edit-event.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/edit-event.js';
$minifier->minify($minifiedPath);

/* event-details.js */
$sourcePath = './assets/js/event-details.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/event-details.js';
$minifier->minify($minifiedPath);

/* live-update.js */
$sourcePath = './assets/js/live-update.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/live-update.js';
$minifier->minify($minifiedPath);

/* my-events.js */
$sourcePath = './assets/js/my-events.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/my-events.js';
$minifier->minify($minifiedPath);

/* profile.js */
$sourcePath = './assets/js/profile.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/profile.js';
$minifier->minify($minifiedPath);

/* register.js */
$sourcePath = './assets/js/register.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/register.js';
$minifier->minify($minifiedPath);

/* set_src_href.js */
$sourcePath = './assets/js/set_src_href.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/set_src_href.js';
$minifier->minify($minifiedPath);

/* settings.js */
$sourcePath = './assets/js/settings.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/settings.js';
$minifier->minify($minifiedPath);

/* googlemaps.js */
$sourcePath = './assets/js/googlemaps.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/googlemaps.js';
$minifier->minify($minifiedPath);

/* timepicker.js */
$sourcePath = './assets/js/timepicker.js';
$minifier = new Minify\JS($sourcePath);

$minifiedPath = '../dist/assets/js/timepicker.js';
$minifier->minify($minifiedPath);


echo 'Minified Successfully';