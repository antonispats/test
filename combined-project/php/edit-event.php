<?php
	session_start();

	include 'includes/functions.php';

	if (!is_user_logged_in()) {
		head("../index.php");
	}

	// Requirements
	include 'config.php';
	require 'configCloud.php';
	require 'settingsCloud.php';

	/*		Error Handling		*/
	if (isset($_GET['error'])) {
		$errorMessage = $_GET['error'];
		switch ($errorMessage) {
            case "sl":
                $_SESSION['errorMessage'] = "Image is too large!";
                break;
            case "stl":
                $_SESSION['errorMessage'] = "An event cannot end without having been started yet!";
                break;
            case "sdl":
                $_SESSION['errorMessage'] = "An event cannot end without having been started yet!";
                break;
            default:
                header("Location: edit-event.php?error");
                exit();
        }
	}
	/*		Error Handling		*/


	$event_name = $image_old = $location_old = $type_old = $category_old = $start_date_old = $end_date_old = $start_time_old = $end_time_old = $description_old = $tickets_old = "";

	if (isset($_GET['event'])) {
		$event_id = $_GET['event'];
		$event_name = get_event_name_by_event_id($conn, $event_id);

		$stmt = $conn->prepare
    	(
    		"SELECT `image`, `location`, `type`, `category`, `start_date`, `end_date`, `description`, `tickets`, `start_time`, `end_time`
			FROM `events`
			WHERE `id`=?"
		);

		mysqli_stmt_bind_param($stmt, 'i', $event_id);
    	mysqli_stmt_execute($stmt);

    	$result = mysqli_stmt_get_result($stmt);

	    if(mysqli_num_rows($result) > 0) {
	        while ($data = mysqli_fetch_assoc($result)) {
	            $image_old = $data['image'];
				$location_old = $data['location'];
				$type_old = $data['type'];
				$category_old = $data['category'];
				$start_date_old = $data['start_date'];
				$end_date_old = $data['end_date'];
				$description_old = $data['description'];
				$tickets_old = $data['tickets'];
				$start_time_old = $data['start_time'];
				$end_time_old = $data['end_time'];
	        }
	    }
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$files = $_FILES["files"];
		$new_event_name = $_POST['name'];
		$location = $_POST['location'];
		$type = $_POST['type'];
		$category = $_POST['category'];
		$start_date = $_POST['start-date'];
		$end_date = $_POST['end-date'];
		$start_time = $_POST['start-time'];
		$end_time = $_POST['end-time'];
		$tickets = $_POST['tickets'];
		$description = $_POST['description'];

		$startDate_formatted = date('Y m d', strtotime($start_date));
		$endDate_formatted = date('Y m d', strtotime($end_date));

		if ($files["size"] > 300000000) {
			header("Location: edit-event.php?event=$event_name&error=sl");
		}  else if ($startDate_formatted >= $endDate_formatted) {
			if ($startDate_formatted == $endDate_formatted) {
				$startTime_formatted = date('H:i:s',strtotime($start_time));
				$endTime_formatted = date('H:i:s',strtotime($end_time));

				if ($startTime_formatted >= $endTime_formatted) {
					header("Location: edit-event.php?event_id=$event_id&error=stl");
					exit();
				}
			}

			header("Location: edit-event.php?event_id=$event_id&error=sdl");
			exit();
		} else {
			if (!empty($_FILES['files']['name'])) {
				// Upload Image to Cloudinary
				$files = is_array($files) ? $files : array( $files );
				$result = upload_file('events', $files["tmp_name"], $files["name"]);
				$imagePublicId = $result['public_id'];
				// Delete Old image
				\Cloudinary\Uploader::destroy($image_old, $options = array("resource_type" => "image"));
			} else {
				$imagePublicId = $image_old;
			}

			if (empty($new_event_name)) {
				$new_event_name = $event_name;
			} if (empty($location)) {
				$location = $location_old;
			} if (empty($type)) {
				$type = $type_old;
			} if (empty($category)) {
				$category = $category_old;
			} if (empty($start_date)) {
				$start_date = $start_date_old;
			} if (empty($end_date)) {
				$end_date = $end_date_old;
			} if (empty($start_time)) {
				$start_time = $start_time_old;
			} if (empty($end_time)) {
				$end_time = $end_time_old;
			} if (empty($description)) {
				$description = $description_old;
			} if (empty($tickets)) {
				$tickets = $tickets_old;
			}

			edit_event($imagePublicId, $new_event_name, $location, $type, $category, $start_date, $end_date, $start_time, $end_time, $description, $tickets, $event_name);
		}
	}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<title>University Opportunities | Edit Event</title></head>
<body>
	<div class="wrapper">
		<?php include 'includes/control-panel.php'; ?>

		<div class="main-panel" id="main-panel">
			<?php include 'includes/nav.php'; ?>

			<div class="content">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6">
						<small><h5 class="card-title">The following card will be appeared in events feed.</h5></small>
						<div class="card">
							<?php
								$img = array(
									'public_id' => $image_old,
								);
								echo cl_image_tag(
									$img['public_id'],
									array("alt" => "Event Image", "id" => "image_preview", "format" => "jpg", "width" => "373", "height" => "200", "crop" => "fit"));
							?>

							<div class="card-body">
								<p class="card-title" style="color:red;">
									<?php
									$start_date_formatted = date('d F Y', strtotime($start_date_old));
									$start_time_formatted = date('g:i A', strtotime($start_time_old));

									echo "<span id='previewDate'>$start_date_formatted</span>&nbsp;at&nbsp;<span id='previewTime'>$start_time_formatted</span>";
									?>
								</p>
								<h5 id="previewName" class="card-title"><?php echo htmlspecialchars($event_name, ENT_QUOTES, 'UTF-8'); ?></h5>
								<p id="previewLocation" class="card-title" style="color:grey;"><?php echo htmlspecialchars($location_old, ENT_QUOTES, 'UTF-8'); ?></p>
								<span id="defaultSpan"><span>
									<?php
										if (!empty($tickets_old)) {
											echo "<u>$tickets_old</u>";
										} else {
											echo "No tickets available for this event.";
										}
									?>
								</span></span>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="card card-user">
							<div class="card-header">
								<h5 class="card-title">Edit Event</h5>
							</div>
							<div class="card-body">
								<form action="edit-event.php?event=<?php echo htmlspecialchars($_GET['event'], ENT_QUOTES, 'UTF-8')?>" method="POST" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-12 pr-1">
											<label for="exampleFormControlFile1">Click here to change event image</label>
											<input name="files" type="file" class="form-control-file" id="eventImage" multiple accept="image/gif, image/jpeg, image/png">
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-6 pr-1">
											<div class="form-group">
												<label>Event Name <small>(required)</small></label>
												<input id="nameID" oninput="liveUpdateName()" name="name" type="text" class="form-control" maxlength="30" autocomplete="off" value="<?php echo htmlspecialchars($event_name, ENT_QUOTES, 'UTF-8'); ?>" required>
											</div>
										</div>
										<div class="col-md-6 pl-1">
											<div class="form-group">
												<label>Location <small>(required)</small></label>
												<input id="locationID" oninput="liveUpdateEventLocation()" name="location" type="text" class="form-control" maxlength="47" autocomplete="off" value="<?php echo htmlspecialchars($location_old, ENT_QUOTES, 'UTF-8'); ?>" required>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6 pr-1">
											<div class="form-group">
												<label>Event Category <small>(required)</small></label>
												<select name="category" class="form-control" id="exampleFormControlSelect1" required>
													<option value="Business">Business & Economics</option>
													<option value="Finance">Banking & Finance</option>
													<option value="Marketing">Marketing & HR</option>
													<option value="Management">Management & Logistics</option>
													<option value="IT">IT & Technology</option>
													<option value="Science">Science & Research</option>
													<option value="Medical">Medical & Pharma</option>
													<option value="Agriculture">Agriculture & Energy</option>
													<option value="Environment">Environment & Wast</option>
													<option value="Travel">Travel & Tourism</option>
												</select>
											</div>
										</div>
										<div class="col-md-6 pl-1">
											<div class="form-group">
												<label>Event Type <small>(required)</small></label>
												<select name="type" class="form-control" id="exampleFormControlSelect1" required>
													<option value="Seminar">Seminar</option>
													<option value="Workshop">Workshop</option>
													<option value="Entrepreneurship">Entrepreneurship Competition</option>
													<option value="Funding">Funding</option>
													<option value="Training">Training Program</option>
													<option value="Networking">Networking </option>
													<option value="Charity">Charity</option>
													<option value="Hosting">Hosting & Empowerment Program</option>
													<option value="Educational">Educational Courses & Training</option>
													<option value="Hackathon">Hackathon</option>
													<option value="Bootcamp">Bootcamp</option>
												</select>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-3 pr-1">
											<div class="form-group">
												<label>Start Date <small>(required)</small></label>
												<input id="startDateID" oninput="liveUpdateEventDate()" name="start-date" type="date" class="form-control" value="<?php echo $start_date_old; ?>" required>
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>Start Time <small>(required)</small></label>
												<input id="startTimeID" oninput="liveUpdateEventTime()" name="start-time" type="time" class="form-control" value="<?php echo $start_time_old; ?>" required>
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>End Date <small>(required)</small></label>
												<input name="end-date" type="date" class="form-control" value="<?php echo $end_date_old; ?>" required>
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>End Time <small>(required)</small></label>
												<input name="end-time" type="time" class="form-control" value="<?php echo $end_time_old; ?>" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div class="form-group">
												<label>Tickets</label>
												<input id="ticketsID" oninput="liveUpdateEventTickets()" name="tickets" type="text" class="form-control" placeholder="https://... , http://..." value="<?php echo $tickets_old; ?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div class="form-group">
												<label>Description <small>(required)</small></label>
												<textarea name="description" autocomplete="off" class="form-control textarea" maxlength=400 required><?php echo htmlspecialchars($description_old, ENT_QUOTES, 'UTF-8'); ?></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div style="color:red; font-size: 1.2em; font-family: 'Calibri-Light';" class="form-group">
												<?php
													if (isset($_SESSION['errorMessage'])) {
								                        echo $_SESSION['errorMessage'];
								                        unset($_SESSION['errorMessage']);
								                    }
												?>
											</div>
										</div>
									</div>
									<button type="submit" onclick="checkFields()" class="btn btn-info" id="settings-button">PUBLISH</button>
									<a href="my-events.php" class="btn btn-danger" id="cancel-button">CANCEL</a>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="../assets/js/edit-event.js"></script>
	<script src="../assets/js/sidebar.js"></script>
	<script src="../assets/js/live-update.js"></script>
</body>
</html>
