<?php
session_start();

include 'includes/functions.php';

if (!is_user_logged_in()) {
	head("../index.php");
}

// Requirements
include 'config.php';
require 'configCloud.php';
require 'settingsCloud.php';

$defaultImage = "Default";

/*		Error Handling		*/
if (isset($_GET['error'])) {
	$errorMessage = $_GET['error'];
	switch ($errorMessage) {
		case "sl":
		$_SESSION['errorMessage'] = "Image is too large!";
		break;
		case "stl":
		$_SESSION['errorMessage'] = "An event cannot end without having been started yet!";
		break;
		case "sdl":
		$_SESSION['errorMessage'] = "An event cannot end without having been started yet!";
		break;
		default:
		header("Location: create-event.php?error");
		exit();
	}
}
/*		Error Handling		*/


if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$files = $_FILES["files"];
	$event_name = $_POST['name'];
	$location = $_POST['location'];
	$type = $_POST['type'];
	$category = $_POST['category'];
	$start_date = $_POST['start-date'];
	$end_date = $_POST['end-date'];
	$start_time = $_POST['start-time'];
	$end_time = $_POST['end-time'];
	$tickets = $_POST['tickets'];
	$description = $_POST['description'];
	// Format Dates
	$startDate_formatted = date('Y m d', strtotime($start_date));
	$endDate_formatted = date('Y m d', strtotime($end_date));
	// Format Times
	$startTime_formatted = date('H:i:s',strtotime($start_time));
	$endTime_formatted = date('H:i:s',strtotime($end_time));

	if ($files["size"] > 300000000) {
		header("Location: create-event.php?error=sl");
	} else if ($startDate_formatted >= $endDate_formatted) {
		if ($startDate_formatted == $endDate_formatted) {
			if ($startTime_formatted >= $endTime_formatted) {
				head("create-event.php?error=stl");
				exit();
			}
		} else {
			head("create-event.php?error=sdl");
			exit();
		}
	}
	//Check if image file is a actual image or fake image
	if (!empty($_FILES['files']['name'])) {
		// Upload Image to Cloudinary and return image id
		$imagePublicId = upload_to_cloud('events', $files);
	} else {
		// set default image, if not picked
		$imagePublicId = "events/".$defaultImage;
	}

	$id = generate_unique_id();

	insert_event($id, $imagePublicId, $event_name, $location, $type, $category, $start_date, $end_date, $startTime_formatted, $endTime_formatted, $description, $tickets);
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<!-- Timepicker -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	<title>University Opportunities | Create Event</title>
	<style>
	#map {
		height: 8cm;
		width: auto;
	}
	</style>
</head>
<body>
	<div class="wrapper">
		<?php include 'includes/control-panel.php'; ?>

		<div class="main-panel" id="main-panel">
			<?php include 'includes/nav.php'; ?>

			<div class="content">
				<div class="row">
					<div class="col-md-4">
						<div class="card">
							<!-- add default image -->
							<img style="margin: 0 auto" id="image_preview" src="https://res.cloudinary.com/uni-ties/image/upload/v1570815006/events/<?php echo $defaultImage ?>.png" alt="Event image will be appeared here." />

							<div class="card-body">
								<p class="card-title" style="color:red;">
									<span id="previewDate"></span>
									<span id="at"></span>
									<span id="previewTime"></span>
								</p>
								<h5 id="previewName" class="card-title"></h5>
								<p id="previewLocation" class="card-title" style="color:grey;"></p>
								<span id="defaultSpan"><span></span></span>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div id="map"></div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="card card-user">
							<div class="card-header">
								<h5 class="card-title">Create Event</h5>
							</div>
							<div class="card-body">
								<form id='create-event-form' action="create-event.php" method="POST" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-12 pr-1">
											<div>
												<label>Event Image</label>
												<input name="files" type="file" class="form-control-file" id="eventImage" multiple accept="image/gif, image/jpeg, image/png">
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-6 pr-1">
											<div class="form-group">
												<label>Event Name <small>(required)</small></label>
												<input id="nameID" oninput="liveUpdateName()" name="name" type="text" class="form-control" maxlength="29" autocomplete="off" required>
											</div>
										</div>
										<div class="col-md-6 pl-1">
											<div class="form-group">
												<label>Location <small>(required)</small></label>
												<input id="locationID" oninput="liveUpdateEventLocation()" name="location" type="text" class="form-control" maxlength="47" autocomplete="off" required>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6 pr-1">
											<div class="form-group">
												<label>Event Category <small>(required)</small></label>
												<select name="category" class="form-control" id="exampleFormControlSelect1" required>
													<option value="Business">Business & Economics</option>
													<option value="Finance">Banking & Finance</option>
													<option value="Marketing">Marketing & HR</option>
													<option value="Management">Management & Logistics</option>
													<option value="IT">IT & Technology</option>
													<option value="Science">Science & Research</option>
													<option value="Medical">Medical & Pharma</option>
													<option value="Agriculture">Agriculture & Energy</option>
													<option value="Environment">Environment & Wast</option>
													<option value="Travel">Travel & Tourism</option>
													<option value="Erasmus">Erasmus+</option>
												</select>
											</div>
										</div>
										<div class="col-md-6 pl-1">
											<div class="form-group">
												<label>Event Type <small>(required)</small></label>
												<select name="type" class="form-control" id="exampleFormControlSelect1" required>
													<option value="Seminar">Seminar</option>
													<option value="Workshop">Workshop</option>
													<option value="Entrepreneurship">Entrepreneurship Competition</option>
													<option value="Funding">Funding</option>
													<option value="Training">Training Course</option>
													<option value="Networking">Networking </option>
													<option value="Charity">Charity</option>
													<option value="Hosting">Hosting & Empowerment Program</option>
													<option value="Educational">Educational Courses & Training</option>
													<option value="Hackathon">Hackathon</option>
													<option value="Bootcamp">Bootcamp</option>
													<option value="YouthEx">Youth Exchange</option>
												</select>
											</div>
										</div>
									</div>

									<!-- check if start date is before end date -->
									<div class="row">
										<div class="col-md-3 pr-1">
											<div class="form-group">
												<label>Start Date <small>(required)</small></label>
												<input id="startDateID" oninput="liveUpdateEventDate()" name="start-date" type="date" class="form-control" required>
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>Start Time <small>(required)</small></label> <br>
												<input id="startTimeID" class="timepicker form-control" oninput="liveUpdateEventTime()" type="text" name="start-time" required readonly>
												<!-- <input id="startTimeID" class="timepicker" oninput="liveUpdateEventTime()" name="start-time" class="form-control" required> -->
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>End Date <small>(required)</small></label>
												<input id="endDateID" name="end-date" type="date" class="form-control" required>
											</div>
										</div>
										<div class="col-md-3 pl-1">
											<div class="form-group">
												<label>End Time <small>(required)</small></label>
												<input id="endTimeID" name="end-time" type="text" class="timepicker form-control" required readonly>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div class="form-group">
												<label>Tickets</label>
												<input id="ticketsID" oninput="liveUpdateEventTickets()" name="tickets" type="text" class="form-control" placeholder="https://... , http://...">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div class="form-group">
												<label>Description <small>(required)</small></label>
												<textarea id="descriptionID" name="description" class="form-control textarea" autocomplete="off" required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 pr-1">
											<div style="color:red; font-size: 1.2em; font-family: 'Calibri-Light';" class="form-group">
												<?php
												if (isset($_SESSION['errorMessage'])) {
													echo $_SESSION['errorMessage'];
													unset($_SESSION['errorMessage']);
												}
												?>
											</div>
										</div>
									</div>
									<button type="submit" onclick="checkFields()" class="btn btn-info" id="settings-button">PUBLISH</button>
									<a href="../index.php" class="btn btn-danger" id="cancel-button">CANCEL</a>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="../assets/js/create-event.js"></script>
	<script src="../assets/js/sidebar.js"></script>
	<script src="../assets/js/live-update.js"></script>
	<script src="../assets/js/timepicker.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	<script src="../assets/js/googlemaps.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAw306iy2vk-kSnIlMMuPlnRRAXuvs-e_Y&libraries=places&callback=initAutocomplete"
	async defer></script>
</body>
</html>
