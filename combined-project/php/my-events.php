<?php
	session_start();

	include 'includes/functions.php';

	if (!is_user_logged_in()) {
		head("../index.php");
	}

	// Requirements
	include 'config.php';
	require 'configCloud.php';
	require 'settingsCloud.php';

	$user_name = get_user_name();

	# Delete Event (With Steps)
	if (isset($_GET["del"])) {
		$event_name = $_GET["del"];

		delete_event($event_name);
	}

	# Fetch Events
	// Check if Filter is Applied
	if (isset($_POST["filter"])) $searchFilter = $_POST["filter"];

	// Get Current Date (Server Date)
	$timezone = date_default_timezone_get('Europe/Athens');
	$date = date('Y-m-d', time());

	if (isset($searchFilter) && $searchFilter == 'upcoming') {
		// Fetch Upcoming Events
		$stmt = $conn->prepare
    	(
    		"SELECT `image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `description`
			FROM `events`, `org_events`
			WHERE `org_events`.`org_name` = ?
			AND `org_events`.`event` = `name`
			AND `events`.`start_date` > ? "
		);

		mysqli_stmt_bind_param($stmt, "ss", $user_name, $date);
    	mysqli_stmt_execute($stmt);

		$result = mysqli_stmt_get_result($stmt);

		if(!$result) {
			echo $conn->error;
		} else {
			$events = array();
		}

		if(mysqli_num_rows($result) > 0) {
			while ($data = mysqli_fetch_assoc($result)) {
				$events[] = $data;
			}
		}
	} else if (isset($searchFilter) && $searchFilter == 'outdated') {
		// Fetch Outdated Events
		$stmt = $conn->prepare
    	(
    		"SELECT `image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `start_time`, `end_time`, `description`
			FROM `events`, `org_events`
			WHERE `org_events`.`org_name` = ?
			AND `org_events`.`event` = `name`
			AND `events`.`end_date` < ? "
		);

		mysqli_stmt_bind_param($stmt, "ss", $user_name, $date);
    	mysqli_stmt_execute($stmt);

		$result = mysqli_stmt_get_result($stmt);

		if(!$result) {
			echo $conn->error;
		}
		$events = array();

		if(mysqli_num_rows($result) > 0) {
			while ($data = mysqli_fetch_assoc($result)) {
				$events[] = $data;
			}
		}
	} else {
		// Fetch All Events
		$stmt = $conn->prepare
    	(
    		"SELECT `id`, `image`, `name`, `location`, `type`, `category`, `start_date`, `end_date`, `start_time`, `end_time`, `description`, `tickets`
			FROM `events`, `org_events`
			WHERE `org_events`.`org_name` = ? AND `org_events`.`event` = `name`;"
		);

		mysqli_stmt_bind_param($stmt, "s", $user_name);
    	mysqli_stmt_execute($stmt);

		$result = mysqli_stmt_get_result($stmt);
		$events = array();

		if(mysqli_num_rows($result) > 0) {
			while ($data = mysqli_fetch_assoc($result)) {
				$events[] = $data;
			}
		}
	}
?>



<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<title>University Opportunities | My Events</title>
</head>
<body>
	<!-- Control Panel -->
	<div class="wrapper">
		<?php include 'includes/control-panel.php'; ?>
		<!-- Navigation Bar -->
		<div class="main-panel" id="main-panel">
			<?php include 'includes/nav.php'; ?>

			<div class="content" id="my-events">
				<?php
					$index = 1;
					$last = 0;

					foreach ($events as $event) {
						if ($index - $last == 1) echo "<div class='row'>";
						?>
						<!-- Events Card -->
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card event" style="width: auto;">
								<?php $link = "../events/".$event["id"]; ?>

								<a href="<?php echo $link ?>">
									<?php
										$img = array(
											'public_id' => $event["image"],
										);
										echo cl_image_tag(
											$img['public_id'],
											array("format" => "jpg", "width" => "448", "height" => "200", "crop" => "fill")
										);
									?>
								</a>
								<div class="card-body">
									<p class="card-title" style="color:red;">
										<?php
										$start_date_formatted = date('d F Y', strtotime($event["start_date"]));
										$start_time_formatted = date('g:i A', strtotime($event["start_time"]));

										echo $start_date_formatted . " at " . $start_time_formatted;
										?>
									</p>
									<h5 class="card-title"><?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8'); ?></h5>
									<p class="card-title" style="color: grey;"><?php echo htmlspecialchars($event["location"], ENT_QUOTES, 'UTF-8'); ?></p>
									<?php $link = "../edit-event/".$event["id"] ?>
									<a href="<?php echo $link; ?>" class="btn btn-info" id="settings-button">Edit</a>
									<button onclick="openModal(<?php echo $index ?>)" type="button" class="btn btn-danger" id="cancel-button">Delete
								</div>
							</div>
						</div>

						<!-- Delete Modal Window -->
						<div class="modal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						    <div class="modal-dialog" role="document">
						        <div class="modal-content">
						            <div class="modal-header">
						                <h5>Delete Confirmation</h5>
						                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                    <span aria-hidden="true">&times;</span>
						                </button> -->
						            </div>
						            <div class="modal-body">
						                <b>Warning: </b>'<?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8'); ?>' will be permanently deleted.<br>Are you sure you want to proceed?
						            </div>
						            <div class="modal-footer">
						                <button onclick="location.href='my-events.php?del=<?php echo htmlspecialchars($event["name"], ENT_QUOTES, 'UTF-8') ?>'" type="submit" class="btn btn-danger" id="cancel-button">Delete</button>
						                <button onclick="closeModal(<?php echo $index ?>)" type="button" class="btn btn-primary" data-dismiss="modal" id="settings-button">Cancel</button>
						            </div>
						        </div>
						    </div>
						</div>

						<!-- BUG WHEN TWO MODALS EXIST AT THE SAME PAGE -->
						<?php
						if ($index % 4 == 0) {
							echo "</div>";
							$last = $index;
						}
						$index++;
					}

					// style missing
					if ($index == 1) {
						echo "
						<div style='margin: 25% auto; text-align: center; width: 20%; color: #808080'>
							<p>You have no events</p>
							<hr>
							<a class='btn btn-primary' href='create-event.php' role='button'><i class='fas fa-plus'></i>&nbsp;Create Event</a>
						</div>
						";
					}
				?>
				<div class="row" id="lds-ring">
					<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
				</div>
			</div>
			<?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script src="../assets/js/eventsAJAX.js"></script>
	<script src="../assets/js/my-events.js"></script>
	<script src="../assets/js/sidebar.js"></script>
</body>
</html>
