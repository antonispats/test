<?php
	session_start();

	include 'includes/functions.php';

	// Requirements
	include 'config.php';
	require 'configCloud.php';
	require 'settingsCloud.php';

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<title>University Opportunities | Privacy Policy</title>
</head>
<body>
	<div class="wrapper">
    <?php
    if (is_user_logged_in()) {
      # user is logged in:

      include 'includes/control-panel.php';
    } else {
      # user is not logged in:

      include 'includes/login-form.php';
    }
    ?>

		<div class="main-panel">
			<?php include 'includes/nav.php'; ?>
      <div class="content">
        <div class="row">
            <div class="container">
              <section>
                <h3 style="color: #787C90; text-align: center; font-weight: bold;">Privacy Policy</h3>

                <article style="font-size: 18px; color: #787C90;">
                  <p style="font-weight: bold;">This document provides information in relation to the privacy and data protection related practices of University Opportunities. It applies to all users of University Opportunities / Uni.ties. Please ensure that you read the terms of the policy in full before continuing to use our website. Please contact us at the address below in case you have any questions or concerns in relation to any of the below.</p>

                  <p style="font-weight: bold; color: #787C90;">Date of Revision: 25 August 2019</p>
                  <h5 style="font-weight: bold;">Preliminary</h5>
                  <p>University Opportunities respect your privacy and are committed to protecting it through our compliance with this privacy policy. This policy describes:</p>

                  <ul>
                    <li>the types of information that we may collect from you when you access or use our websites, applications and other online services (collectively, our "Services"); and</li>
                    <li>our practices for collecting, using, maintaining, protecting and disclosing that information.</li>
                  </ul>
                  <p><strong>Application of this Policy:</strong> This policy applies only to information we collect through our Services, in email, voice, call, text and other electronic communications sent through or in connection with our Services.</p>
                  <p><strong>What does this Policy not Apply to:</strong> This policy <strong>DOES NOT</strong> apply to information that you provide to, or that is collected by, any third-party, such as organizers, events, data providers and social networks that you use in connection with our Services. We encourage you to consult directly with such third-parties about their privacy practices.</p>
                  <p><strong>Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, you have a choice not to use or discontinue using our Services. By accessing or using our Services, you agree to this privacy policy.</strong></p>
                  <p><strong>Updates to this Policy:</strong> This policy may change from time to time, your continued use of our Services after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</p>

                  <h5 style="font-weight: bold;">I. The information we collect and how we use it</h5>
                  <p>We collect several types of information from and about users of our Services, including information:</p>
                  <ul>
                    <li>by which you may be personally identified; and/or</li>
                    <li>about your internet connection, the equipment you use to access our Services and your usage details which may include browsing history.</li>
                  </ul>
                  <p>We collect this information:</p>
                  <ul>
                    <li>directly from you when you provide it to us; and/or</li>
                    <li>automatically as you navigate through our Services (information collected automatically may include usage details, IP addresses and information collected through cookies, web beacons and other tracking technologies).</li>
                  </ul>

                  <h5 style="font-weight: bold;">Information You Provide to Us</h5>
                  <p>The information we collect on or through our Services may include:</p>
                  <ul>
                    <li><strong>Your account information:</strong> Your full name, email address, postal code, password and other information you may provide with your account, such as your gender, mobile phone number and website. Your profile picture that will be publicly displayed as part of your account profile. You may optionally provide us with this information through third-party sign-in services such as Facebook, LinkedIn and Google. In such cases, we fetch and store whatever information is made available to us by you through these sign-in services.</li>
                    <li><strong>Your preferences:</strong> Your preferences and settings such as time zone and language.</li>
                    <li><strong>Your content:</strong> Information you provide through our Services, including your reviews, photographs, comments, lists, followers, the users/companies you follow or connected, events you follow or attended or plan to attend, details and history, favorites and bookmarks, networking preferences and associated history, contact information of people you add to, invite/share or notify, your event visits and bookings through our Services, names, and other information you provide on our Services, and other information in your account profile.</li>
                    <li><strong>Your searches and other activities:</strong> The search terms you have looked up and results you selected.</li>
                    <li><strong>Your browsing information:</strong> How long you used our Services and which features you used; the ads you clicked on.</li>
                    <li><strong>Your communications:</strong> Communications between you and other users or companies through our Services; your participation in a survey, poll, feedbacks; your request for certain features (e.g., newsletters, updates or other products); your communication with us about employment opportunities posted to the services.</li>
                    <li><strong>Your Public Posts:</strong> You also may provide information (such as ratings, reviews, tips, photos, comments, likes, bookmarks, friends, lists, etc.) to be published or displayed (hereinafter, "posted") on publicly accessible areas of our Services, or transmitted to other users of our Services or third-parties (collectively, "User Contributions"). Your User Contributions are posted on and transmitted to others at your own risk. Please be aware that no security measures are perfect or impenetrable. Additionally, we cannot control the actions of other users of our Services with whom you may choose to share your User Contributions. Therefore, we cannot and do not guarantee that your User Contributions will not be viewed by unauthorized persons. We may display this information on the Services, share it with businesses, and further distribute it to a wider audience through third party sites and services. You should be careful about revealing any sensitive details about yourself in such postings.
                    </li>
                  </ul>

                  <p>We use the information you provide to us to enhance the functionality and improve the quality of our Services, and to personalize your experience while using our Services. We also use this information to display relevant advertising, provide support to you, communicate with you, and comply with our legal obligations.</p>
                  <p><strong>Please ensure that you have all the requisite permissions and authorizations to share any information with us. It is your sole responsibility to ensure that you have obtained the necessary consents in relation to any information which may relate to third parties apart from yourself.</strong></p>
                  <br />

                  <h5 style="font-weight: bold;">Information We Collect Through Automatic Data Collection Technologies</h5>
                  <p>We may automatically collect certain information about the computer or devices (including mobile devices) you use to access the Services, and about your use of the Services, even if you use the Services without registering or logging in.</p>
                  <ul>
                    <li><strong>Usage information:</strong> Details of your use of our Services, including traffic data, location data, logs and other communication data and the resources that you access and use on or through our Services.</li>
                    <li><strong>Computer and device information:</strong> Information about your computer, Internet connection and mobile device, including your IP address, operating systems, platforms, browser type, other browsing information (connection, speed, connection type etc.), device type, device’s unique device identifier, mobile network information and the device’s telephone number.</li>
                    <li><strong>Stored information and files:</strong> Our applications also may access metadata and other information associated with other files stored on your mobile device. This may include, for example, photographs, audio and video clips, personal contacts and address book information.</li>
                    <li><strong>Location information:</strong> Our applications collect real-time information about the location of your device, as permitted by you.</li>
                    <li><strong>Last URL visited:</strong> The URL of the last web page you visited before visiting our websites.</li>
                    <li><strong>Mobile device IDs:</strong> Unique mobile device identifier (e.g. IDFA or other device IDs on Apple devices like the iPhone and iPad), if you’re using our Services on a mobile device, we may use mobile device IDs (the unique identifier assigned to a device by the manufacturer), instead of cookies, to recognize you. We may do this to store your preferences and track your use of our applications. Unlike cookies, mobile device IDs cannot be deleted. Advertising companies may use device IDs to track your use of our applications, track the number of advertisements displayed, measure advertising performance and display advertisements that are more relevant to you. Analytics companies may use mobile device IDs to track your usage of our applications.</li>
                    <li><strong>Your preferences:</strong> Your preferences and settings such as time zone and language.</li>
                    <li><strong>Your activity on the Services:</strong> Information about your activity on the Services, such as your search queries, comments, domain names, search results selected, number of clicks, pages viewed and the order of those pages, how long you visited our Services, the date and time you used the Services, error logs, and other similar information.</li>
                    <li><strong>Mobile status:</strong> For mobile application users, the online or offline status of your application.</li>
                  </ul>

                  <h5 style="font-weight: bold;">Precise Location Information and How to Opt Out</h5>
                  <p>When you use one of our location-enabled services (for example, when you access Services from a mobile device), we may collect and process information about your mobile device’s GPS location (including the latitude, longitude or altitude of your mobile device) and the time the location information is recorded to customize the Services with location-based information and features (for example, to inform you about events in your area). Some of these services require your personal data for the feature to work and we may associate location data with your device ID and other information we hold about you.</p>

                  <h5 style="font-weight: bold;">How Long Do we Keep this Data?</h5>
                  <p>We keep this data for no longer than is reasonably necessary for providing services to you. If you wish to use the particular feature, you will be asked to consent to your data being used for this purpose. You can withdraw your consent at any time by disabling the GPS or other location-tracking functions on your device, provided your device allows you to do this.</p>
                  <br />

                  <h5 style="font-weight: bold;">Cookies and Other Electronic Tools</h5>

                  <h5 style="font-weight: bold;">Do we use Cookies?</h5>
                  <p>We, and third parties with whom we partner, may use cookies, pixel tags, web beacons, mobile device IDs, “flash cookies” and similar files or technologies to collect and store information in respect to your use of the Services and third party websites.</p>

                  <h5 style="font-weight: bold;">What is a Cookie?</h5>
                  <p>A cookie is a small text file that is stored on your computer that enables us to recognize you (for example, as a registered user) when you visit our website, store your preferences and settings, enhance your experience by delivering content and advertising specific to your interests, perform research and analytics, track your use of our Services, and assist with security and administrative functions. Cookies may be persistent or stored only during an individual session. A pixel tag (also called a web beacon or clear GIF) is a tiny graphic with a unique identifier, embedded invisibly on a webpage (or an online ad or email), and is used to count or track things like activity on a webpage or ad impressions or clicks, as well as to access cookies stored on users’ computers. 10times uses pixel tags to measure the popularity of our various pages, features and services. We also may include web beacons in e-mail messages or newsletters to determine whether the message has been opened and for other analytics.
                  </p>

                  <h5 style="font-weight: bold;">How can Cookies be disabled?</h5>
                  <p>Most browsers are set to automatically allow cookies. Please note it may be possible to disable some (but not all) cookies through your device or browser settings, but doing so may interfere with certain functionality on the Services. Major browsers provide users with various options when it comes to cookies. Users can usually set their browsers to block all third-party cookies (which are those set by third-party companies collecting information on websites operated by other companies), block all cookies (including first-party cookies such as the ones 10times uses to collect search activity information about its users), or block specific cookies. To modify your cookie settings, please visit your browser’s help settings. You will need to opt out on each browser and each device you use to access the Services. Flash cookies operate differently than browser cookies and cannot be removed or blocked via web browser settings. By using our Services with your browser set to accept cookies you are consenting to our use of cookies.
                  </p>
                  <p>Third parties whose products or services are accessible or advertised through the Services, including social media services, may also use cookies or similar tools, and we advise you to check their privacy policies for information about their cookies and other practices. We do not control the practices of such partners and their privacy policies govern their interactions with you.</p>

                  <h5 style="font-weight: bold;">Information from Third Parties</h5>
                  <p>We may collect, process and store your user ID associated with any social media account (such as your Facebook, LinkedIn and Google account) that you use to sign into the Services or connect with or use with the Services.</p>
                  <p>When you sign in to your account with your social media account information, or otherwise connect to your social media account with the Services, you consent to our collection, storage, and use, in accordance with this Privacy Policy, of the information that you make available to us through the social media interface. This could include, without limitation, any information that you have made public through your social media account, information that the social media service shares with us, or information that is disclosed during the sign-in process. Please see your social media provider’s privacy policy and help center for more information about how they share information when you choose to connect your account.</p>
                  <p>We may also obtain information about you from third parties such as partners, marketers, third-party websites, and researchers, and combine that information with information which we collect from or about you.</p>

                  <h5 style="font-weight: bold;">Anonymous or De-Identified Data</h5>
                  <p>We may anonymize and/or de-identify information collected from you through the Services or via other means, including via the use of third-party web analytic tools as described below. As a result, our use and disclosure of aggregated and/or de-identified information is not restricted by this Privacy Policy, and it may be used and disclosed to others without limitation.</p>
                  <br />

                  <h5 style="font-weight: bold;">II. How we use the information we collect</h5>
                  <p>We use the information we collect from and about you for a variety of purposes, including to:</p>
                  <ul>
                    <li>Process and respond to your queries.</li>
                    <li>Understand our users (what they do on our Services, what features they like, how they use them, etc.), improve the content and features of our Services (such as by personalizing content to your interests), process and complete your actions, and make special offerings.</li>
                    <li>Administer our Services and diagnose technical problems.</li>
                    <li>Send you communications that you have requested or that may be of interest to you by way of emails, or courier, or registered post, or telephone calls, or any other mode of communication. We may also share your preferences or the Services availed by you with your network followers on Uni.ties for marketing and other promotional activities of our Services.</li>
                    <li>Enable us to show you ads that are relevant to you.</li>
                    <li>Generate and review reports and data about, and to conduct research on, our user base and Service usage patterns.</li>
                    <li>Provide you with customer support.</li>
                    <li>Provide you with notices about your account.</li>
                    <li>Notify you about changes to our Services.</li>
                    <li>Allow you to participate in interactive features offered through our Services.</li>
                    <li>In any other way we may describe when you provide the information.</li>
                  </ul>
                  <p>We may also use your information to contact you about our own and third-party goods and services that may be of interest to you. If you do not want us to use your information in this way, please check the relevant box located on the form on which we collect your data and/or adjust your user preferences in your account profile.</p>
                  <p>We may use the information we have collected from you to enable us to display advertisements to our advertisers’ target audiences. Even though we do not disclose your personal information for these purposes without your consent, if you click on or otherwise interact with an advertisement, the advertiser may assume that you meet its target criteria.</p>
                  <br />

                  <h5 style="font-weight: bold;">III. Legal Basis for Processing Your Information</h5>
                  <p>We rely on the following legal grounds to process your personal information:</p>
                  <ul>
                    <li><strong>Consent.</strong>We may use your personal information as described in this Policy subject to your consent. To withdraw your consent, please contact us at <a href="mailto:uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a> . You may also refrain from providing, or withdraw, your consent for cookies. Please see our clause providing information about user rights and choices below for more information on opt-outs.</li>
                    <li><strong>Legitimate Interests.</strong>We may use your personal information for our legitimate interests to provide our Platform and services and to improve our services and the content on our Platform. We process information on behalf of our customers who have legitimate interests in operating their businesses. We may use technical information as described in this Policy and use personal information for our marketing purposes consistent with our legitimate interests and any choices that we offer or consents that may be required under applicable law.</li>
                  </ul>
                  <br />

                  <h5 style="font-weight: bold;">IV. How we share the information we collect</h5>
                  <p>We may disclose personal information that we collect or you provide, as described in this privacy policy, in the following ways:</p>
                  <h5 style="font-weight: bold;">General Information Disclosures</h5>
                  <ul>
                    <li>To contractors, service providers and other third-parties whom we use to support our business (e.g. ticketing providers, event organizers, event reservations) and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.</li>
                    <li>To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of Uni.ties assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal information held by Uni.ties about the users of our Services are among the assets transferred.</li>
                    <li>To third-parties to market their products or services to you if you have consented to receive promotional updates. We contractually require these third-parties to keep personal information confidential and use it only for the purposes for which we disclose it to them. You have the right to contact us to discontinue these messages.</li>
                    <li>To fulfill the purpose for which you provide it.</li>
                    <li><strong>Legal Purposes.</strong>We may share your information when we believe in good faith that such sharing is reasonably necessary in order to investigate, prevent, or take action regarding possible illegal activities or to comply with legal process. We may also share your information to investigate and address threats or potential threats to the physical safety of any person, to investigate and address violations of this Privacy Policy or the Terms of Service , or to investigate and address violations of the rights of third parties and/or to protect the rights, property and safety of University Opportunities, our employees, users, or the public. This may involve the sharing of your information with law enforcement, government agencies, courts, and/or other organizations on account of legal request such as subpoena, court order or government demand to comply with the law. You understand that we may not be able to notify you of such disclosure if we are not permitted to do so under applicable law.
                    </li>
                    <li><strong>Social Networks.</strong>If you interact with social media features on our Services, such as the Facebook Like button, or use your social media credentials to log-in or post content, these features may collect information about your use of the Services, as well as post information about your activities on the social media service. Your interactions with social media companies are governed by their privacy policies. We are going to share your events on our Facebook and Instagram page in order to attract more audience.</li>
                    <li><strong>Consent.</strong>We may share your information in any other circumstances where we have your consent.</li>
                  </ul>
									<br />

									<h5 style="font-weight: bold;">V. Choices about how we use and disclose your information</h5>
									<p>We strive to provide you with choices regarding the personal information you provide to us. You can set your browser or mobile device to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe’s website.</p>
									<p>If you disable or refuse cookies, please note that some parts of our Services may then be inaccessible or not function properly.</p>
									<br />

									<h5 style="font-weight: bold;">VI. Communications choices</h5>
									<p>When you sign up for an account, you are opting in to receive emails, texts, calls, notifications from other University Opportunities users, businesses, and University Opportunities itself. You can log in to manage your email preferences and you can follow the “unsubscribe” instructions in commercial email messages, but note that you cannot opt out of receiving certain administrative notices, service notices, or legal notices from University Opportunities.</p>
									<br />

									<h5 style="font-weight: bold;">VII. User Rights and Reviewing, changing or deleting information</h5>
									<h5 style="font-weight: bold;">Account</h5>
									<p>In order to keep your personal information accurate and complete, you can log in to review and update your account information via your account settings page. You may also contact us to request information about the personal data we have collected from you and to request the correction, modification or deletion of such personal information. We will do our best to honor your requests subject to any legal and contractual obligations. If you would like to make a request, cancel your account or request we delete or no longer use your account information to provide you Services, contact us at <a href="mailto:uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a> . Subject to applicable law, we will retain and use your account information only as necessary to comply with our legal obligations, resolve disputes and enforce our agreements.</p>
									<p>If you delete your User Contributions from our websites, copies of your User Contributions may remain viewable in cached and archived pages, or might have been copied or stored by other users of our websites. Proper access and use of information provided on our websites, including User Contributions, is governed by our Terms of Use.</p>
									<p>Please also contact us at <a href="mailto:uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a>  to receive a copy of the information we hold about you in a reasonably acceptable format that you may use to port to another service. However, each request will be evaluated on a case-by-case basis and will remain subject to the exceptions available under applicable law.</p>

									<h5 style="font-weight: bold;">E-mail</h5>
									<p>As described above, if you do not wish to receive promotional emails from us, you may opt out at any time by following the opt-out link contained in the email itself. Please note that it may take up to ten (10) days to process your request. Please also note that if you opt out of receiving marketing communications from us, we may continue to send to you service-related emails which are not available for opt-out. If you do not wish to receive any service-related emails from us, you have the option to deactivate your account.</p>

									<h5 style="font-weight: bold;">Cookies</h5>
									<p>You may also refrain from providing, or withdraw, your consent for cookies. Your browser’s help function should contain instructions on how to set your computer to accept all cookies, to notify you when a cookie is issued, or to not receive cookies at any time.</p>

									<h5 style="font-weight: bold;">Third Party Analytics Services</h5>
									<p>Some of the services used provide the ability to opt-out. You may do so using Opt-Out Features on their respective websites.</p>
									<br />

									<h5 style="font-weight: bold;">VIII. Accessing & correcting your personal information</h5>
									<p>We will take reasonable steps to accurately record the personal information that you provide to us and any subsequent updates.</p>
									<p>We encourage you to review, update, and correct the personal information that we maintain about you, and you may request that we delete personal information about you that is inaccurate, incomplete, or irrelevant for legitimate purposes, or are being processed in a way which infringes any applicable legal requirement.</p>
									<p>Your right to review, update, correct, and delete your personal information may be limited, subject to the applicable law of your jurisdiction: (i) if your requests are abusive or unreasonably excessive, (ii) where the rights or safety of another person or persons would be encroached upon, or (iii) if the information or material you request relates to existing or anticipated legal proceedings between you and us, or providing access to you would prejudice negotiations between us or an investigation of possible unlawful activity. Your right to review, update, correct, and delete your information is subject to our records retention policies and applicable law, including any statutory retention requirements.</p>
									<br />

									<h5 style="font-weight: bold;">IX. Security: How we protect your information</h5>
									<p>We have implemented industry-standard appropriate physical, electronic, and managerial procedures to safeguard and help prevent unauthorized access to your information and to maintain data security.</p>
									<p>These safeguards take into account the sensitivity of the information that we collect, process and store and the current state of technology. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it.</p>
									<p>However, no method of transmission over the Internet or via mobile device, or method of electronic storage, is 100% secure. Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security and thereby usage in a manner that is inconsistent with this Privacy Policy.</p>
									<p>We assume no liability or responsibility for disclosure of your information due to errors in transmission, unauthorized third-party access, or other causes beyond our control. You play an important role in keeping your personal information secure. You should not share your user name, password, or other security information for your Uni.ties account with anyone. If we receive instructions using your user name and password, we will consider that you have authorized the instructions.</p>
									<p>In case you would like to know more about how we keep your information secure, please feel free to reach out to us at <a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a>  with your specific queries.</p>
									<br />

									<h5 style="font-weight: bold;">X. Children Under 18</h5>
									<p>University Opportunities strongly believes in protecting the privacy of children. In line with this belief, we do not knowingly collect or maintain Personally Identifiable Information on our Site from persons under 16 years of age, and no part of our Site is directed to persons under 16 years of age. If you are under 16 years of age, then please do not use or access our services at any time or in any manner. We will take appropriate steps to delete any Personally Identifiable Information of persons less than 16 years of age that has been collected on our Site without verified parental consent upon learning of the existence of such Personally Identifiable Information.</p>
									<p>If we become aware that a person submitting personal information is under 16, we will delete the account and all related information as soon as possible. If you believe we might have any information from or about a child under 16, please contact us at <a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a>.</p>
									<br />

									<h5 style="font-weight: bold;">XI. Third-Party Links</h5>
									<p>Our Platform may contain links to third-party websites and applications. Subject to your opt-out preferences (see Choices about how we use and disclose your information), we may also use third-party advertisers, ad networks, and other advertising, marketing, and promotional companies, to serve advertisements on our websites. Any access to and use of such linked websites and applications is not governed by this Policy but instead is governed by the privacy policies of those third parties. We do not endorse these parties, their content, or any products and services they offer, and we are not responsible for the information practices of such third-party websites or applications.</p>
									<br />

									<h5 style="font-weight: bold;">XII. Compliance with data protection regulations and Data Transfers</h5>
									<p>Personal Information that we collect may be transferred to, and stored at, any of our affiliates, partners or service providers which may be inside or outside the country you resides in / Economic area your country is part of. By submitting your personal data, you agree to such transfers. Your Personal Information may be transferred to countries that do not have the same data protection laws as the country in which you initially provided the information. When we transfer or disclose your Personal Information to other countries, we will protect that information as described in this Privacy Policy. relevant, we will ensure appropriate contractual safeguards to ensure that your information is processed with the highest standards of transparency and fairness.</p>
									<p>We comply with generally accepted industry standard regulations regarding the collection, use, and retention of data. Each location may provide for different data protection rules than the country in which you reside. If you have any questions relating to your personal data, please write to us on <a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a>.</p>
									<br />

									<h5 style="font-weight: bold;">XIII. Data retention and account termination</h5>
									<p>Ordinarily, we will retain your information for as long as necessary to provide you with our Services. The length of time we retain Personally Identifiable Information for depends on the purposes for which we collect and use it and/or as required to comply with applicable laws and to establish, exercise or defend our legal rights.</p>
									<p>You can close your account by visiting your profile settings page on our website or by writing to us at <a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a> .  We will remove your public posts from view and/or dissociate them from your account profile, but we may retain information about you for the purposes authorized under this Privacy Policy unless prohibited by law.</p>
									<p>Thereafter, within a reasonable period (generally not exceeding 90 days), we will either delete your personal information or de-identify it so that it is anonymous and not attributed to your identity. For example, we may retain certain information to prevent, investigate, or identify possible wrongdoing in connection with the Service or to comply with legal obligations.</p>
									<br />
									<br />

									<h3 style="color: #787C90; text-align: center; font-weight: bold;">Terms of Service</h3>
									<br />

									<h5 style="font-weight: bold;">I. Acceptance of terms</h5>
									<p>Thank you for using University Opportunities.</p>
									<p>Please read these Terms carefully. By accessing or using the University Opportunities Platform, you are agreeing to these Terms.</p>
									<p>You may not be able to use the Services if you do not accept the Terms or are unable to be bound by the Terms. Your use of the University Opportunities Platform is at your own risk, including the risk that you might be exposed to content that is objectionable, or otherwise inappropriate.</p>
									<p>In order to use the Services, you must first agree to the Terms. You can accept the Terms by:</p>
									<ul>
										<li>Clicking to accept or agree to the Terms, where it is made available to you by University Opportunities in the user interface for any particular Service; or</li>
										<li>Actually using the Services. In this case, you understand and agree that University Opportunities will treat your use of the Services as acceptance of the Terms from that point onwards.</li>
									</ul>
									<p>We hold the sole right to modify the Terms of Service without prior permission from You or providing notice to You. The relationship creates on You a duty to periodically check the Terms of Service and stay updated on its requirements. If You continue to use the Website or avail any of its services without registration following such change, this is deemed as consent by You to the so amended policies. Your continued use of the Website is conditioned upon your compliance with the Terms of Service, including but not limited to compliance with the Terms of Service even after alterations, if any.</p>
									<br />

									<h5 style="font-weight: bold;">II. Definitions</h5>
									<ul>
										<li><strong>“Agreement”</strong> shall mean and refer to this Terms of Service, including any amendments that may be incorporated into it.</li>
										<li><strong>“User”</strong> or <strong>“You”</strong> or <strong>“Your”</strong> refers to you, as a user of the Services. A user is someone who accesses or uses the Services for the purpose of sharing, displaying, hosting, publishing, transacting, or uploading information or views or pictures and includes other persons jointly participating in using the Services including without limitation a user having access to ‘business page’ to manage claimed business listings or otherwise.</li>
										<li><strong>“Content”</strong> will include (but is not limited to) reviews, images, photos, audio, video, location data, nearby places, and all other forms of information or data. "Your content" or "User Content" means content that you upload, share or transmit to, through or in connection with the Services, such as likes, ratings, reviews, images, photos, messages, profile information, and any other materials that you publicly display or displayed in your account profile. "Uni.ties Content" means content that Uni.ties creates and make available in connection with the Services including, but not limited to, visual interfaces, interactive features, graphics, design, compilation, computer code, products, software, aggregate ratings, reports and other usage-related data in connection with activities associated with your account and all other elements and components of the Services excluding Your Content and Third Party Content. "Third Party Content" means content that comes from parties other than Uni.ties or its users and is available on the Services.
										</li>
									</ul>
									<br />

									<h5 style="font-weight: bold;">III. Eligibility to use the services</h5>
									<ul>
										<li>You hereby represent and warrant that you are at least eighteen (18) years of age or above and are fully able and competent to understand and agree the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms.</li>
										<li>Compliance with Laws. You are in compliance with all laws and regulations in the country in which you live when you access and use the Services. You agree to use the Services only in compliance with these Terms and applicable law, and in a manner that does not violate our legal rights or those of any third party(ies).</li>
									</ul>
									<br />

									<h5 style="font-weight: bold;">IV. Changes in the terms</h5>
									<p>University Opportunities may vary or amend or change or update these Terms, from time to time entirely at its own discretion. You shall be  responsible for checking these Terms from time to time and ensure continued compliance with these Terms. Your use of University Opportunities  Platform after any such amendment or change in the Terms shall be deemed as your express acceptance to such amended/changed terms and you also agree to be bound by such changed/amended Terms.</p>
									<br />

									<h5 style="font-weight: bold;">V. Provision of the services being offered</h5>
									<ul>
										<li>University Opportunities is constantly evolving in order to provide the best possible experience and information to its users. You acknowledge and agree that the form and nature of the Services which University Opportunities provides, may require effecting certain changes in it, therefore, University Opportunities reserves the right to suspend/cancel, or discontinue any or all products or services at any time without notice, make modifications and alterations in any or all of its contents, products and services contained on the site without any prior notice.</li>
										<li>You acknowledge and agree that if University Opportunities  disables access to your account, you may be prevented from accessing the Services, your account details or any files or other content, which is contained in your account.</li>
										<li>You acknowledge and agree that while University Opportunities may not currently have set a fixed upper limit on the number of transmissions you may send or receive through the Services, University Opportunities may set such fixed upper limits at any time, at University Opportunities discretion.</li>
										<li>University Opportunities reserves the right to charge subscription and/or membership fees from a user, by giving reasonable prior notice, in respect of any product, service or any other aspect of the University Opportunities Platform anytime in future.</li>
									</ul>

									<h5 style="font-weight: bold;">CALL, SMS MESSAGES AND EMAIL NOTIFICATIONS</h5>
									<ul>
										<li>As part of the Services, University Opportunities may send reminders, alerts (further referred to as 'notifications'), via call, SMS text message, app notification or email to its users, and you acknowledge and consent to the receipt of such messages. University Opportunities may use one or more carriers for delivering such messages & communications.</li>
										<li>You certify that you are the account holder of the mobile phone number provided to University Opportunities  or that you have the account holder's express permission to use the specified phone to receive text messages about your registration & other communication from University Opportunities.</li>
										<li>With each registration, you may receive a standard call, SMS messages, app notifications or emails relating to your registrations or University Opportunities service communications. Message and data rates may apply.</li>
										<li>You acknowledge and agree that the reception of the call, SMS messages, app notifications and emails is not 100% guaranteed and that you are responsible for the timely response. You further acknowledge and agree that the reception of the call, SMS messages, app notifications and emails is dependent on the operation of your mobile phone provider and/or internet service provider and the service of the mobile phone company and/or internet service provider with which you have an account or prepaid card. University Opportunities shall have no responsibility or liability for the damages and costs incurred by your not receiving a call, SMS message, app notifications or email on time or by the insufficient operation of your mobile network, mobile phone company and/or internet service provider.</li>
										<li>You may opt out of receiving the call, SMS text messages, app notifications or email notifications at any time. Opting out of call, SMS, email or app notification can be completed within the Uni.ties website. To stop the messages from coming to your mobile, you may opt out by sending mail to <a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a> with Your Phone No.</li>
									</ul>
									<br />

									<h5 style="font-weight: bold;">VI. Disclaimer</h5>
									<p>By using University Opportunities Services you agree to the following disclaimer: The Content on these Services is for informational purposes only. University Opportunities  disclaims any liability for any information that may have become outdated or incorrect since the last time the particular piece of information was updated. The University Opportunities Platform is merely a venue where Users may act as Customers or Service Providers. University Opportunities  is not a party to any Service Contracts between Customers and Service Providers unless otherwise stated. University Opportunities further does not guarantee entry to the event by using some of the services as part of University Opportunities services like actions on event to follow, attend, going. It merely act as a engagement tool to enrich the experience of platform. University Opportunities makes full effort to make sure that these actions are well addressed by the representations of events and businesses by passing your requests to them but has no control over it. University Opportunities reserves the right to make changes and corrections to any part of the Content on these Services at any time without prior notice. University Opportunities does not guarantee quality of the information, accuracy of information like event dates, venue, timings, pricing or any other information. Unless stated otherwise, all pictures and information contained on these Services are believed to be owned by or licensed to University Opportunities or have been provided by third party under third party license agreement.. Please email a takedown request (by using the “Contact Us” link on the home page) to the webmaster if you are the copyright owner of any Content on these Services and you think the use of the above material violates Your copyright in any way. Please indicate the exact URL of the webpage in your request. All images shown here have been digitized by University Opportunities. No other party is authorized to reproduce or republish these digital versions in any format whatsoever without the prior written permission of University Opportunities.
									</p>
									<br />

									<h5 style="font-weight: bold;">VII. Use of services by you or user</h5>
									<p>University Opportunities  User Account Including ‘Claim Your Listing’ Access</p>
									<p>You must create an account in order to use some of the features offered by the Services, including without limitation to ‘claim your business listing’ on the Services. Use of any personal information you provide to us during the account creation process is governed by our <a href="#privacy" style="color: #787C90; text-decoration: underline;">Privacy Policy</a>. You must keep your password confidential and you are solely responsible for maintaining the confidentiality and security of your account, all changes and updates submitted through your account, and all activities that occur in connection with your account.</p>
									<p>You may also be able to register to use the Services by logging into your account with your credentials from certain third party social networking sites (e.g., Facebook, Linkedin, Google). You confirm that you are the owner of any such social media account and that you are entitled to disclose your social media login information to us. You authorize us to collect your authentication information, and other information that may be available on or through your social media account consistent with your applicable settings and instructions.</p>
									<p>In creating an account and/or claiming your business’ listing, you represent to us that all information provided to us in such process is true, accurate and correct, and that you will update your information as and when necessary in order to keep it accurate. If you are creating an account or claiming a business listing, then you represent to us that you are the owner or authorized agent of such business. You may not impersonate someone else, create or use an account for anyone other than yourself, provide an email address other than your own, create multiple accounts or business listings except as otherwise authorized by us, or provide or use false information to obtain access to a business’ listing on the Services that you are not legally entitled to claim. You acknowledge that any false claiming of a business listing may cause University Opportunities or third parties to incur substantial economic damages and losses for which you may be held liable and accountable.</p>
									<p>You are also responsible for all activities that occur in your account. You agree to notify us immediately of any unauthorized use of your account in order to enable us to take necessary corrective action. You also agree that you will not allow any third party to use your University Opportunities account for any purpose and that you will be liable for such unauthorized access.</p>
									<p>By creating an account, you agree to receive certain communications in connection with University Opportunities Platform or Services. For example, you might receive comments, connection request, messages from other Users or other users may follow the activity to do on your account. You can opt-out or manage your preferences regarding non-essential communications through account settings or by contacting us.</p>
									<br />

									<h5 style="font-weight: bold;">Others Terms</h5>
									<p>You agree to use the Services only for purposes that are permitted by (a) the Terms and (b) any applicable law, regulation or generally accepted practices or guidelines in the relevant jurisdictions.</p>
									<p>You agree to use the data owned by University Opportunities (as available on the Services or through any other means like API etc.) only for personal use/purposes and not for any commercial use (other than in accordance with ‘Claim Your Listing’ access) unless agreed to by/with University Opportunities in writing.</p>
									<p>You agree not to access (or attempt to access) any of the Services by any means other than the interface that is provided by Uni.ties, unless you have been specifically allowed to do so, by way of a separate agreement with Uni.ties. You specifically agree not to access (or attempt to access) any of the Services through any automated means (including use of scripts or web crawlers) and shall ensure that you comply with the instructions set out in any robots.txt file present on the Services.</p>
									<p>You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services). You shall not delete or revise any material or information posted by any other User(s), shall not engage in spamming, including but not limited to any form of emailing, posting or messaging that is unsolicited.</p>
									<br />

									<h5 style="font-weight: bold;">VIII. Content</h5>

									<h5 style="font-weight: bold;">Ownership of University Opportunities Content and Proprietary Rights</h5>
									<p>We are the sole and exclusive copyright owners of the Services and our Content. We also exclusively own the copyrights, trademarks, service marks, logos, trade names, trade dress and other intellectual and proprietary rights throughout the world (the “IP Rights”) associated with the Services and University Opportunities  Content, which may be protected by copyright, patent, trademark and other applicable intellectual property and proprietary rights and laws. You acknowledge that the Services contain original works and have been developed, compiled, prepared, revised, selected, and arranged by us and others through the application of methods and standards of judgment developed and applied through the expenditure of substantial time, effort, and money and constitutes valuable intellectual property of us and such others. You further acknowledge that the Services may contain information which is designated as confidential by University Opportunities  and that you shall not disclose such information without University Opportunities  prior written consent.
									</p>
									<p>You agree to protect University Opportunities proprietary rights and the proprietary rights of all others having rights in the Services during and after the term of this agreement and to comply with all reasonable written requests made by us or our suppliers and licensors of content or otherwise to protect their and others’ contractual, statutory, and common law rights in the Services. You acknowledge and agree that University Opportunities own all legal right, title and interest in and to the Services, including any IP Rights which subsist in the Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist). You further acknowledge that the Services may contain information which is designated as confidential by University Opportunities  and that you shall not disclose such information without University Opportunities prior written consent. Unless you have agreed otherwise in writing with University Opportunities nothing in the Terms gives you a right to use any of University Opportunities trade names, trademarks, service marks, logos, domain names, and other distinctive brand features.
									</p>
									<p>You agree not to use any framing techniques to enclose any trademark or logo or other proprietary information of University Opportunities; or remove, conceal or obliterate any copyright or other proprietary notice or source identifier, including without limitation, the size, color, location or style of any proprietary mark(s). Any infringement shall lead to appropriate legal proceedings against you at appropriate forum for seeking all available/possible remedies under applicable laws of the country of violation. You cannot modify, reproduce, publicly display or exploit in any form or manner whatsoever any of the University Opportunities Content in whole or in part except as expressly authorized by University Opportunities.</p>
									<p>To the fullest extent permitted by applicable law, we neither warrant nor represent that your use of materials displayed on the Services will not infringe rights of third parties not owned by or affiliated with us. You agree to immediately notify us upon becoming aware of any claim that the Services infringe upon any copyright trademark, or other contractual, intellectual, statutory, or common law rights.</p>
									<br />

									<h5 style="font-weight: bold;">Your License to University Opportunities Content</h5>
									<p>We grant you a personal, limited, non-exclusive and non-transferable license to access and use the Services only as expressly permitted in these Terms. You shall not use the Services for any illegal purpose or in any manner inconsistent with these Terms. You may use information made available through the Services solely for your personal, non-commercial use. You agree not to use, copy, display, distribute, modify, broadcast, translate, reproduce, reformat, incorporate into advertisements and other works, sell, promote, create derivative works, or in any way exploit or allow others to exploit any of University Opportunities Content in whole or in part except as expressly authorized by us. Except as otherwise expressly granted to you in writing, we do not grant you any other express or implied right or license to the Services, University Opportunities Content or our IP Rights.</p>
									<p>Any violation by you of the license provisions contained in this Section may result in the immediate termination of your right to use the Services, as well as potential liability for copyright and other IP Rights infringement depending on the circumstances.</p>

									<h5 style="font-weight: bold;">University Opportunities  License to Your or User Content</h5>
									<p>By submitting Your Content you hereby irrevocably grant University Opportunities a perpetual, irrevocable, world-wide, non-exclusive, royalty-free, sub-licensable and transferable license and right to use Your Content (including content shared by any business user having access to a ‘event page or business page’ to manage claimed business listings or otherwise) and all IP Rights therein for any purpose including API partnerships with third parties and in any media existing now or in future. By “use” we mean use, copy, display, distribute, modify, translate, reformat, incorporate into advertisements and other works, promote, create derivative works, and in the case of third party services, allow their users and others to do the same. You grant us the right to use the name or username that you submit in connection with Your Content. You irrevocably waive, and cause to be waived, any claims and assertions of moral rights or attribution with respect to Your Content brought against University Opportunities  or its Users, any third party services and their users.
									</p>

										<h5 style="font-weight: bold;">Representations Regarding Your or User Content</h5>
										<p>You are responsible for Your Content. You represent and warrant that you are the sole author of, own, or otherwise control all of the rights of Your Content or have been granted explicit permission from the rights holder to submit Your Content; Your Content was not copied from or based in whole or in part on any other content, work, or website; Your Content was not submitted via the use of any automated process such as a script bot; use of Your Content by us, third party services, and our and any third party users will not violate or infringe any rights of yours or any third party; Your Content is truthful and accurate; and Your Content does not violate the Guidelines and Policies or any applicable laws. If Your Content is a review, you represent and warrant that you are the sole author of that review; the review reflects an actual dining experience that you had; you were not paid or otherwise remunerated in connection with your authoring or posting of the review; and you had no financial, competitive, or other personal incentive to author or post a review that was not a fair expression of your honest opinion.
										</p>
										<p>You assume all risks associated with Your Content, including anyone's reliance on its quality, accuracy, or reliability, or any disclosure by you of information in Your Content that makes you personally identifiable. While we reserve the right to remove Content, we do not control actions or Content posted by our users and do not guarantee the accuracy, integrity or quality of any Content. You acknowledge and agree that Content posted by users and any and all liability arising from such Content is the sole responsibility of the user who posted the content, and not University Opportunities.</p>

										<h5 style="font-weight: bold;">Content Removal</h5>
										<p>We reserve the right, at any time and without prior notice, to remove, block, or disable access to any content that we, for any reason or no reason, consider to be objectionable, in violation of the Terms or otherwise harmful to the Services or our users in our sole discretion. Subject to the requirements of applicable law, we are not obligated to return any of Your Content to you under any circumstances. Any review, which is derogatory, defamatory or hateful or in violation of the Guidelines and Polices and without any substantial evidence may be taken down at our sole discretion.</p>

										<h5 style="font-weight: bold;">Third Party Content and Links</h5>
										<p>Some of the content available through the Services may include or link to materials that belong to third parties, such as third party hotel reservation services, event badging or ticketing. Please note that your use of such third party services will be governed by the terms of service and <a href="#privacy" style="color: #787C90; text-decoration: underline;">privacy policy</a> applicable to the corresponding third party. We may obtain business addresses, phone numbers, and other contact information from third party vendors who obtain their data from public sources. We have no control over, and make no representation or endorsement regarding the accuracy, relevancy, copyright compliance, legality, completeness, timeliness or quality of any product, services, advertisements and other content appearing in or linked to from the Services. We do not screen or investigate third party material before or after including it on our Services. We reserve the right, in our sole discretion and without any obligation, to make improvements to, or correct any error or omissions in, any portion of the content accessible on the Services. Where appropriate, we may in our sole discretion and without any obligation, verify any updates, modifications, or changes to any content accessible on the Services, but shall not be liable for any delay or inaccuracies related to such updates. You acknowledge and agree that University Opportunities is not responsible for the availability of any such external sites or resources, and does not endorse any advertising, products or other materials on or available from such web sites or resources.
										</p>
										<p>Third party content, including content posted by our users, does not reflect our views or that of our parent, subsidiary, affiliate companies, branches, employees, officers, directors, or shareholders. In addition, none of the content available through the Services is endorsed or certified by the providers or licensors of such third party content. We assume no responsibility or liability for any of Your Content or any third party content. You further acknowledge and agree that University Opportunities is not liable for any loss or damage which may be incurred by you as a result of the availability of those external sites or resources, or as a result of any reliance placed by you on the completeness, accuracy or existence of any advertising, products or other materials on, or available from, such web sites or resources. Without limiting the generality of the foregoing, we expressly disclaim any liability for any offensive, defamatory, illegal, invasive, unfair, or infringing content provided by third parties.
										</p>

										<h5 style="font-weight: bold;">User Reviews</h5>
										<p>User reviews or ratings for events, venues, businesses do not reflect the opinion of University Opportunities. University Opportunities receives multiple reviews or ratings by users, which reflect the opinions of the Users. It is pertinent to state that each and every review posted on University Opportunities is the personal opinion of the user/reviewer only. University Opportunities is a neutral platform, which solely provides a means of communication between users/reviewers including users or event/venue/businesses owners/representatives with access to business page. The advertisements published on the University Opportunities Platform are independent of the reviews received by such advertisers. We are a neutral platform and we don’t arbitrate disputes, however in case if someone writes a review that the listing owner/representative does not consider to be true, the best option for the representative would be to contact the reviewer or post a public response in order to clear up any misunderstandings. If the representative believes that any particular user’s review violates any of the University Opportunities policies, the representative may write to us at
										<a href="uni.ties18@gmail.com" style="color: #787C90; text-decoration: underline;">uni.ties18@gmail.com</a> and bring such violation to our attention. University Opportunities may remove the review in its sole discretion if review is in violation of the Terms, or content guidelines and policies or otherwise harmful to the Services.</p>
										<br />

										<h5 style="font-weight: bold;">IX. Content guidelines and privacy policy</h5>

										<h5 style="font-weight: bold;">Content Guidelines</h5>
										<p>You represent that you have read, understood and agreed to our Guidelines and Policies related to Content.</p>

										<br />

										<p>You represent that you have read understood and agreed to our <a href="#privacy" style="color: #787C90; text-decoration: underline;">Privacy Policy</a>. Please note that we may disclose information about you to third parties or government authorities if we believe that such a disclosure is reasonably necessary to (i) take action regarding suspected illegal activities; (ii) enforce or apply our Terms and Privacy Policy; (iii) comply with legal process or other government inquiry, such as a search warrant, subpoena, statute, judicial proceeding, or other legal process/notice served on us; or (iv) protect our rights, reputation, and property, or that of our users, affiliates, or the general public.</p>
										<br />

										<h5 style="font-weight: bold;">X. Restrictions on use</h5>
										<p>Without limiting the generality of these Terms, in using the Services, you specifically agree not to post or transmit any content (including review) or engage in any activity that, in our sole discretion:</p>
										<ul>
											<li>Violate our Guidelines and Policies;</li>
											<li>Is harmful, threatening, abusive, harassing, tortious, indecent, defamatory, discriminatory, vulgar, profane, obscene, libelous, hateful or otherwise objectionable, invasive of another’s privacy, relating or encouraging money laundering or gambling;</li>
											<li>Constitutes an inauthentic or knowingly erroneous review, or does not address the information and services, atmosphere, or other attributes of the business you are reviewing.</li>
											<li>Violate our Guidelines and Policies;</li>
											<li>Contains material that violates the standards of the Services;</li>
											<li>Violates any third-party right, including, but not limited to, right of privacy, right of publicity, copyright, trademark, patent, trade secret, or any other intellectual property or proprietary rights;</li>
											<li>Accuses others of illegal activity, or describes physical confrontations;</li>
											<li>Is illegal, or violates any federal, state, or local law or regulation (for example, by disclosing or trading on inside information in violation of securities law);</li>
											<li>Attempts to impersonate another person or entity;</li>
											<li>Disguises or attempts to disguise the origin of Your Content, including but not limited to by: (i) submitting Your Content under a false name or false pretenses; or (ii) disguising or attempting to disguise the IP address from which Your Content is submitted;</li>
											<li>Constitutes a form of deceptive advertisement or causes, or is a result of, a conflict of interest;</li>
											<li>Is commercial in nature, including but not limited to spam, surveys, contests, pyramid schemes, postings or reviews submitted or removed in exchange for payment, postings or reviews submitted or removed by or at the request of the business being reviewed, or other advertising materials;</li>
											<li>Asserts or implies that Your Content is in any way sponsored or endorsed by us;</li>
											<li>Contains material that is not in English or, in the case of products or services provided in foreign languages, the language relevant to such products or services;</li>
											<li>Falsely states, misrepresents, or conceals your affiliation with another person or entity;</li>
											<li>Accesses or uses the account of another user without permission;</li>
											<li>Distributes computer viruses or other code, files, or programs that interrupt, destroy, or limit the functionality of any computer software or hardware or electronic communications equipment;</li>
											<li>Interferes with, disrupts, or destroys the functionality or use of any features of the Services or the servers or networks connected to the Services;</li>
											<li>“Hacks” or accesses without permission our proprietary or confidential records, records of another user, or those of anyone else;</li>
											<li>Violates any contract or fiduciary relationship (for example, by disclosing proprietary or confidential information of your employer or client in breach of any employment, consulting, or non-disclosure agreement);</li>
											<li>Decompiles, reverse engineers, disassembles or otherwise attempts to derive source code from the Services;</li>
											<li>Removes, circumvents, disables, damages or otherwise interferes with security-related features, or features that enforce limitations on use of, the Services;</li>
											<li>Violates the restrictions in any robot exclusion headers on the Services, if any, or bypasses or circumvents other measures employed to prevent or limit access to the Services;</li>
											<li>Collects, accesses, or stores personal information about other users of the Services;</li>
											<li>Is posted by a bot;</li>
											<li>Harms minors in any way;</li>
											<li>Threatens the unity, integrity, defense, security or sovereignty of India or of the country of use, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation;</li>
											<li>Modifies, copies, scrapes or crawls, displays, publishes, licenses, sells, rents, leases, lends, transfers or otherwise commercialize any rights to the Services or Our Content; or</li>
											<li>Attempts to do any of the foregoing.</li>
										</ul>
										<p>You acknowledge that University Opportunities has no obligation to monitor your – or anyone else’s – access to or use of the Services for violations of the Terms, or to review or edit any content. However, we have the right to do so for the purpose of operating and improving the Services (including without limitation for fraud prevention, risk assessment, investigation and customer support purposes), to ensure your compliance with the Terms and to comply with applicable law or the order or requirement of legal process, a court, consent decree, administrative agency or other governmental body.</p>
										<p>You hereby agree and assure University Opportunities shall be used for lawful purposes only and that you will not violate laws, regulations, ordinances or other such requirements of any applicable Central, Federal State or local government or international law(s). You shall not upload, post, email, transmit or otherwise make available any unsolicited or unauthorized advertising, promotional materials, junk mail, spam mail, chain letters or any other form of solicitation, encumber or suffer to exist any lien or security interest on the subject matter of these Terms or to make any representation or warranty on behalf of University Opportunities in any form or manner whatsoever.</p>
										<p>Any Content uploaded by you, shall be subject to relevant laws of India and of the country of use and may be disabled, or and may be subject to investigation under applicable laws. Further, if you are found to be in noncompliance with the laws and regulations, these terms, or the <a href="#privacy" style="color: #787C90; text-decoration: underline;">privacy policy</a> of the University Opportunities Platform shall have the right to immediately block your access and usage of the University Opportunities Platform and University Opportunities shall have the right to remove any non-compliant content and or comment forthwith, uploaded by you and shall further have the right to take appropriate recourse to such remedies as would be available to it under various statutes.</p>
										<br />

										<h5 style="font-weight: bold;">XI. User feedback</h5>
										<p>If you share or send any ideas, suggestions, changes or documents regarding University Opportunities existing business ("Feedback"), you agree that (i) your Feedback does not contain the confidential, secretive or proprietary information of third parties, (ii) University Opportunities is under no obligation of confidentiality with respect to such Feedback, and shall be free to use the Feedback on an unrestricted basis (iii) University Opportunities  may have already received similar Feedback from some other user or it may be under consideration or in development, and (iv) By providing the Feedback, you grant us a binding, non-exclusive, royalty-free, perpetual, global license to use, modify, develop, publish, distribute and sublicense the Feedback, and you irrevocably waive, against University Opportunities and its users any claims/assertions, whatsoever of any nature, with regard to such Feedback.</p>
										<p>Please provide only specific Feedback on University Opportunities existing products or marketing strategies; do not include any ideas that University Opportunities policy will not permit it to accept or consider.</p>
										<p>Notwithstanding the abovementioned clause, University Opportunities or any of its employees do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, new promotions, new or improved products or technologies, product enhancements, processes, materials, marketing plans or new product names. Please do not submit any unsolicited ideas, original creative artwork, suggestions or other works (“Submissions”) in any form to University Opportunities or any of its employees. The purpose of this policy is to avoid potential misunderstandings or disputes when University Opportunities products or marketing strategies might seem similar to ideas submitted to University Opportunities . If, despite our request to not send us your ideas, you still submit them, then regardless of what your letter says, the following terms shall apply to your Submissions.</p>

										<h5 style="font-weight: bold;">Terms of Idea Submission</h5>
										<p>You agree that: (1) your Submissions and their Contents will automatically become the property of University Opportunities, without any compensation to you; (2) University Opportunities may use or redistribute the Submissions and their contents for any purpose and in any way; (3) there is no obligation for University Opportunities to review the Submission; and (4) there is no obligation to keep any Submissions confidential.</p>
										<br />

										<h5 style="font-weight: bold;">XII. Advertising</h5>
										<p>Some of the Services are supported by advertising revenue and may display advertisements and promotions. These advertisements may be targeted to the content of information stored on the Services, queries made through the Services or other information. The manner, mode and extent of advertising by University Opportunities on the Services are subject to change without specific notice to you. In consideration for University Opportunities granting you access to and use of the Services, you agree that University Opportunities may place such advertising on the Services.</p>
										<p>Part of the site may contain advertising information or promotional material or other material submitted to University Opportunities by third parties or Users. Responsibility for ensuring that material submitted for inclusion on the University Opportunities Platform or mobile apps complies with applicable international and national law is exclusively on the party providing the information/material. Your correspondence or business dealings with, or participation in promotions of, advertisers other than University Opportunities found on or through the University Opportunities Platform and or mobile apps, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, shall be solely between you and such advertiser. University Opportunities will not be responsible or liable for any error or omission, inaccuracy in advertising material or any loss or damage of any sort incurred as a result of any such dealings or as a result of the presence of such other advertiser(s) on the University Opportunities Platform and mobile application.
										</p>
										<br />

										<h5 style="font-weight: bold;">XIV. Termination of your access to the services</h5>
										<p>You can delete your account at any time  by using settings on your profile section.</p>
										<p>We may terminate your use of the Services and deny you access to the Services in our sole discretion for any reason or no reason, including your: (i) violation of these Terms; or (ii) lack of use of the Services. You agree that any termination of your access to the Services may be affected without prior notice, and acknowledge and agree that we may immediately deactivate or delete your account and all related information and/or bar any further access to your account or the Services. If you use the Services in violation of these Terms, we may, in our sole discretion, retain all data collected from your use of the Services. Further, you agree that we shall not be liable to you or any third party for the discontinuation or termination of your access to the Services.</p>
										<br />

										<h5 style="font-weight: bold;">XV. General terms</h5>
										<h5 style="font-weight: bold;">Interpretation:</h5>
										<p>The section and subject headings in these Terms are included for reference only and shall not be used to interpret any provisions of these Terms.</p>

										<h5 style="font-weight: bold;">Entire Agreement and Waiver:</h5>
										<p>The Terms, together with the ‘<a href="#privacy" style="color: #787C90; text-decoration: underline;">Privacy Policy</a>’,, shall constitute the entire agreement between you and us concerning the Services. No failure or delay by us in exercising any right, power or privilege under the Terms shall operate as a waiver of such right or acceptance of any variation of the Terms and nor shall any single or partial exercise by either party of any right, power or privilege preclude any further exercise of that right or the exercise of any other right, power or privilege.</p>

										<h5 style="font-weight: bold;">Severability:</h5>
										<p>If any provision of these Terms is deemed unlawful, invalid, or unenforceable by a judicial court for any reason, then that provision shall be deemed severed from these Terms, and the remainder of the Terms shall continue in full force and effect.</p>

										<h5 style="font-weight: bold;">Governing Law/Waiver:</h5>
										<p>These Terms shall be governed by the laws of Greece .</p>
                </article>
              </section>
          </div>
        </div>
      </div>
      <?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script src="../assets/js/set_src_href.js"></script>
</body>
</html>
