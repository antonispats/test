<?php
	session_start();

	include 'includes/functions.php';

	/*		Error Handling		*/
	if (isset($_GET['error'])) {
		$errorMessage = $_GET['error'];
		switch ($errorMessage) {
			case "pf":
				$_SESSION['errorMessage'] = "<b>Password could not be accepted!</b><br />- Is your password 8-12 characters long?<br />- Does your password contain all the following (at least once): Special character, Number, Uppercase character and Lowercase character?<br />- Make sure you have used only the following special characters:<br />! # $ % & ( ) * + , - . ; = ? @ [ ] ^ _ { } ~<br />- Does your password contain any whitespace?";
				break;
			case "uae":
                $_SESSION['errorMessage'] = "Username already exists! Please choose another one.";
				break;
			case "eae":
				$_SESSION['errorMessage'] = "Email already exist! Please use another email";
				break;	
            case "ieu":
                $_SESSION['errorMessage'] = "Invalid email and username format!";
                break;
            case "ie":
                $_SESSION['errorMessage'] = "Invalid email format!";
                break;
            case "iu":
                $_SESSION['errorMessage'] = "Invalid username format!";
                break;
            case "pdm":
                $_SESSION['errorMessage'] = "Passwords do not match!";
                break;
            default:
                head("register.php");
                exit();
        }
	}
	/*		Error Handling		*/

	// Get form data
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$passwordRepeat = $_POST['passwordRepeat'];
		$category = $_POST['category'];
		$logo = "logos/default-logo";		// set default logo
		$about = "";
		$validation_code = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
		$active = 0;

		$upperCaseCheck         = preg_match('/[A-Z]/'       , $password);                          // Uppercase Letter
        $lowerCaseCheck         = preg_match('/[a-z]/'       , $password);                          // Lowercase Letter
        $numbersCheck           = preg_match('/\d/'          , $password);                          // Number
        $specialCharsAllowed    = preg_match('/[\\!\\#\\$\\%\\&\\(\\)\\*\\+\\,\\-\\.\\/\\=\\?\\@\\[\\\\\]\\^\\_\\{\\}\\~\\/\\\\\'\\/\\:]/'
                                                         	 , $password);                          // Special Characters allowed
        $specialCharsNotAllowed = preg_match('/[\\"\\<\\>\\\\\`\\|\\;]/' , $password);       // Special Characters not allowed
        $whitespaceCheck        = preg_match('/\s/'      	 , $password);                          // Whitespace

        // echo "has upper case: " . $upperCaseCheck;
        // echo "has lower case: " . $lowerCaseCheck;
        // echo "has number: " . $numbersCheck;
        // echo "has special chars: " . $specialCharsAllowed;
        // echo "has prohibited special chars: " . $specialCharsNotAllowed;
        // echo "has whitespace: " . $whitespaceCheck;
        // echo "length: " . strlen($password);


		if (!is_valid_password($password, $upperCaseCheck, $lowerCaseCheck, $numbersCheck, $specialCharsAllowed, $specialCharsNotAllowed, $whitespaceCheck)) {
			head("register.php?error=pf");
		} else if (username_already_exists($name)) {
			head("register.php?error=uae");
		} else if (email_already_exists($email)) {
			head("register.php?error=eae");
		} else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $name)) {
			head("register.php?error=ieu");
		} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			head("register.php?error=ie");
		} else if (!preg_match("/[^a-zA-Z0-9_]*$/", $name)) {
            echo $name;
			//head("register.php?error=iu");
		} else if ($password !== $passwordRepeat) {
			head("register.php?error=pdm");
		} else {
            $id = generate_unique_id();

			register_user($id, $name, $email, $password, $logo, $about, $category, $validation_code, $active);
			register_user_info($name);
		}

		exit();
	}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<title>University Opportunities | Register</title>
</head>
<body>
	<div class="wrapper">
		<div class="main-panel" style="width:100%;">
			<div class="content">
				<div class="row" >
					<div class="col-md-3" style="margin: 0 auto; color: #808080">
						<div class="prof_img" style="text-align:center;">
							<a href="../index.php">
								<img src="../assets/img/unities_extended.png" style="width: 90%;">
							</a>
						</div>
						<form action="register.php" method="POST">
							<div class="form-group">
								<label for="exampleInputName">Name</label>
								<input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">E-mail</label>
								<input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputPassword4">Password</label>
									<input id="password-field" data-toggle="tooltip" data-placement="left" title="" name="password" type="password" class="form-control">
									<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
								<div class="form-group col-md-6">
									<label for="inputCity">Re-type Password</label>
									<input name="passwordRepeat" type="password" class="form-control" id="inputCity">
									<span toggle="#inputCity" class="fa fa-fw fa-eye field-icon toggle-password"></span>
									
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Account Type</label>
								<select name="category" class="form-control" id="exampleFormControlSelect1">
									<option value="MKO">Non-Profit Organization</option>
									<option value="Test1">Voluntary Association</option>
									<option value="Test2">Non-Govermental Organization</option>
									<option value="Test3">Political Organization</option>
									<option value="Test4">Charitable Organization</option>
									<option value="Test4">Professional Association</option>
									<option value="Test4">Society</option>
									<option value="Test4">Community</option>
									<option value="Test4">Mutual Organization</option>
								</select>
							</div>
							<div class="form-group" style="margin: 0">
	                <input type="checkbox" tabindex="3" name="remember" id="remember">
	                <label name="remember" id="remember" for="remember" style="margin: 0;">&nbsp;Yes, i would also like to sign up for the newsletter</label>
	            </div>

							<hr>
							<div class="form-group">
								<small>
									By clicking Sign Up, you agree to our <a href="./privacy" style="color: #787C90;"><u>Terms
									and Data Use Policy</u></a>, including our Cookie Use.
								</small>
							</div>
							<div style="color:red; font-size: 1em; font-family: 'Arial';" class="form-group">
				                <?php
				                    if (isset($_SESSION['errorMessage'])) {
				                        echo $_SESSION['errorMessage'];
				                        unset($_SESSION['errorMessage']);
				                    }
				                ?>
							</div>
							<button type="submit" class="btn btn-primary" style="width:100%;" id="cancel-button">Sign Up</button>
						</form>

						<div style="text-align: center;">
							<p>Already have an account? <a href="../index.php" style="color: #787C90;"><b>Sign in</b></a></p>
						</div>
					</div>
				</div>
			</div>
			<?php include 'includes/footer.php'; ?>
		</div>
	</div>
	
	<?php include 'includes/scripts.php'; ?>
	<script src="../assets/js/register.js"></script>
	</body>
</html>
