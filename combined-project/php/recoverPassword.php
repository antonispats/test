<?php
session_start();

include 'includes/functions.php';
$conn = start_connection_db();

	/*    Error Handling    */
	if (isset($_GET['error'])) {
		$errorMessage = $_GET['error'];
		switch ($errorMessage) {
			case "e":
				$_SESSION['errorMessage'] = "This email does not exist!";
				break;
			default:
				header("Location: recoverPassword.php");
				exit();
		}
	}
	/*    Error Handling    */

	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		if (isset($_SESSION['token']) && ($_POST['token'] === $_SESSION['token'])) {
			$email = mysqli_real_escape_string($conn, $_POST['email']);

			if (email_exists($email)) {
				// Generate validation code
				$validation_code = md5($email);

				// Set Cookie for 'code.php' page expiration date | 15 minutes
				/* ***NOTE: Should inlcude 'setcookie(path, domain, security)' when ready*** */
				setcookie('temp_access_code', $validation_code, time() + 900);

				// Insert validation code into DB
				insert_validation_code_into_db($email, $validation_code);

				send_recover_email($email, $validation_code);

				$_SESSION['resetMessage'] = "<p class='p-3 mb-2 bg-success text-white'>Please check your email or spam folder for a password reset code.</p>";
				header("Location: ../index.php");
			} else {
				header('Location: recoverPassword.php?error=e');
			}
		} else {
			header("Location: ../index.php");
		}
	}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <?php include 'includes/head.php'; ?>
  <title>University Opportunities</title>
  <?php include 'includes/head.php'; ?>
</head>
<body>
  <div class="wrapper">
	<div class="main-panel" style="width:100%;">
	  <div class="content">
		<div class="row" >
		  <div class="col-md-3" style="margin: 0 auto; background-color: white; border-style: solid; border-width: 1px; padding: 15px; margin-top: 5%;">
			<div class="prof_img" style="text-align:center;" id="recoverPassword-section">
				<img src="../assets/img/lock.jpg" style="width: 20%;"><br>
				<h4><b>Password Recovery</b></h4>
				<p>You can reset your password here:</p>
			</div>
			<form action="recoverPassword.php" method="POST">
			  <div class="form-group">
				<input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
			  </div>
				<div style="color:red; font-size: 1.2em; font-family: 'Calibri-Light'; text-align: center;" class="form-group">
					<?php
						if (isset($_SESSION['errorMessage'])) {
							echo $_SESSION['errorMessage'];
							unset($_SESSION['errorMessage']);
						}
					?>
				</div>
			  <button type="submit" class="btn btn-primary" style="width:100%;" id="cancel-button">Reset Password</button>
			  <input type="hidden" name="token" value="<?php echo generate_token(); ?>">
			</form>
		  </div>
		</div>
	  </div>
	  <?php include 'includes/footer.php'; ?>
	</div>
  </div>

  <?php include 'includes/scripts.php'; ?>
</body>
</html>
