<!-- social media nav bar: both -->
<div class="pre-navbar">
  <div class="row">
	<ul class="navbar-nav mr-auto">
	</ul>
	  <a href="https://www.facebook.com/UniversityOpportunities" class="btn btn-sm" id="xshide"><i class="fab fa-facebook-square"></i></a>
	  <a href="https://www.instagram.com/uni.ties" class="btn btn-sm" id="xshide"><i class="fab fa-instagram"></i></a>
	  <a href="#" class="btn btn-blue btn-sm"><i class="fab fa-linkedin"></i></a>
	<!--/.nav-collapse -->
  </div>
</div>
<!-- end of social media nav bar -->