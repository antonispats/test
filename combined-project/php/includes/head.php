<meta charset="utf-8" />
<base href="http://localhost/unities/combined-project/php/includes">
<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
<!-- favicon -->
<link rel="icon" type="image/png" href="../assets/img/fav/apple-icon-57x57.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<!-- Fonts and icons -->
<!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
<!-- CSS Files -->
<link href="../assets/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="../assets/css/paper-dashboard.css" type="text/css" rel="stylesheet" />
<!-- fontawesome -->
<link rel="stylesheet" href="../assets/icons/css/all.min.css">
<!-- <script src="https://kit.fontawesome.com/3ec944184f.js" crossorigin="anonymous"></script> -->
