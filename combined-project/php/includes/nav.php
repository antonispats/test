<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
	<div class="container-fluid">
		<div class="navbar-wrapper">
			<div class="navbar-toggle">
				<button type="button" class="navbar-toggler">
					<span class="navbar-toggler-bar bar1"></span>
					<span class="navbar-toggler-bar bar2"></span>
					<span class="navbar-toggler-bar bar3"></span>
				</button>
			</div>
			<div class="openbtn">
				<button id="openbtn" onclick="openSidebar()">☰</button>
			</div>
			<a id="index-a" href="/" class="navbar-brand" style="margin-right: 1em"><img src="/assets/img/unities_logo.png" class="img-responsive" width="25"/> UNIVERSITY OPPORTUNITIES</a>
		</div>
		<button id="navbar-toggler-bar-id" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-bar navbar-kebab"></span>
			<span class="navbar-toggler-bar navbar-kebab"></span>
			<span class="navbar-toggler-bar navbar-kebab"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end" id="navigation">
			<form id="search-form" action="index.php" method="GET">
				<div class="input-group no-border">
					<input id="search-event-field" type="text" name="q" class="form-control" placeholder="Search...">
					<div class="input-group-append" style="margin-right: 1em">
						<div class="input-group-text">
							<i class="fas fa-search" ></i>
						</div>
					</div>
				</div>
			</form>
			<a class="btn btn-primary" href="https://uni-ties.gr" role="button" target="_blank">Our Blog</a>
			<form id="filter-events-form" action="index.php" method="get">
				<ul class="navbar-nav">
					<li class="nav-item btn-rotate dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><small>Event Category</small></a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" style="padding:5px; width: max-content;">
							<input value="Business" type="checkbox" name="category[]">&nbsp;Business & Economics<br>
							<input value="Finance" type="checkbox" name="category[]">&nbsp;Banking & Finance<br>
							<input value="Marketing" type="checkbox" name="category[]">&nbsp;Marketing & HR<br>
							<input value="Management" type="checkbox" name="category[]">&nbsp;Management & Logistics<br>
							<input value="IT" type="checkbox" name="category[]">&nbsp;IT & Technology<br>
							<input value="Science" type="checkbox" name="category[]">&nbsp;Science & Research<br>
							<input value="Medical" type="checkbox" name="category[]">&nbsp;Medical & Pharma<br>
							<input value="Agriculture" type="checkbox" name="category[]">&nbsp;Agriculture & Energy<br>
							<input value="Environment" type="checkbox" name="category[]">&nbsp;Environment & Waste<br>
							<input value="Travel" type="checkbox" name="category[]">&nbsp;Travel & Tourism<br>
							<input value="Erasmus" type="checkbox" name="category[]">&nbsp;Erasmus+<br>
						</div>
					</li>
					<li class="nav-item btn-rotate dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><small>Event Type</small></a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" style="padding:5px; width: max-content;">
							<input value="Seminar" type="checkbox" name="type[]">&nbsp;Seminar<br>
							<input value="Workshop" type="checkbox" name="type[]">&nbsp;Workshop<br>
							<input value="Entrepreneurship" type="checkbox" name="type[]">&nbsp;Entrepreneurship Competition<br>
							<input value="Funding" type="checkbox" name="type[]">&nbsp;Funding<br>
							<input value="Training" type="checkbox" name="type[]">&nbsp;Training Course<br>
							<input value="Networking" type="checkbox" name="type[]">&nbsp;Networking<br>
							<input value="Charity" type="checkbox" name="type[]">&nbsp;Charity<br>
							<input value="Hosting" type="checkbox" name="type[]">&nbsp;Hosting & Empowerment Program<br>
							<input value="Educational" type="checkbox" name="type[]">&nbsp;Educational Courses & Training<br>
							<input value="Hackathon" type="checkbox" name="type[]">&nbsp;Hackathon<br>
							<input value="Bootcamp" type="checkbox" name="type[]">&nbsp;Bootcamp<br>
							<input value="YouthEx" type="checkbox" name="type[]">&nbsp;Youth Exchange<br>
						</div>
					</li>
					<li class="nav-item btn-rotate dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><small>MONTH</small></a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2" style="padding:5px;">
							<input value="1" type="checkbox" name="month[]">&nbsp;January<br>
							<input value="2" type="checkbox" name="month[]">&nbsp;February<br>
							<input value="3" type="checkbox" name="month[]">&nbsp;March<br>
							<input value="4" type="checkbox" name="month[]">&nbsp;April<br>
							<input value="5" type="checkbox" name="month[]">&nbsp;May<br>
							<input value="6" type="checkbox" name="month[]">&nbsp;June<br>
							<input value="7" type="checkbox" name="month[]">&nbsp;July<br>
							<input value="8" type="checkbox" name="month[]">&nbsp;August<br>
							<input value="9" type="checkbox" name="month[]">&nbsp;September<br>
							<input value="10" type="checkbox" name="month[]">&nbsp;October<br>
							<input value="11" type="checkbox" name="month[]">&nbsp;November<br>
							<input value="12" type="checkbox" name="month[]">&nbsp;December<br>
						</div>
					</li>
					<li class="nav-item btn-rotate dropdown">
						<button type="submit" class="btn btn-info btn-sm" id="search-button-index">Search</button>
					</li>
					<li class="nav-item btn-rotate dropdown">
						<input type="reset" class="btn btn-warning btn-sm" id="resetfilter-button-index" value="Reset Filters">
					</li>
				</ul>
			</form>
		</div>
	</div>
</nav>
