<?php
	$user_name = '';
	if (isset($_SESSION['name'])) {
		$user_name = $_SESSION['name'];
	} else if (isset($_COOKIE['user'])) {
		$user_name = $_COOKIE['user'];
	}
?>

<div class="sidebar" id="sidebar" data-color="white" data-active-color="danger"><br>
	<div class="logo" style="text-align:center;">
		<div class="logo-image-small">
			<?php
				$stmt = $conn->prepare
				(
					"SELECT `id` FROM `accounts` WHERE `name`=?"
				);

				mysqli_stmt_bind_param($stmt, 's', $user_name);
				mysqli_stmt_execute($stmt);

				$result = mysqli_stmt_get_result($stmt);
				$user_id = '';

				if(mysqli_num_rows($result) > 0) {
					while ($data = mysqli_fetch_assoc($result)) {
						$user_id = $data["id"];
					}
				}
			?>

			<?php $link = "../profiles/".$user_id; ?>
			<a href="<?php echo $link; ?>">
			<?php
				$stmt = $conn->prepare
    			(
    				"SELECT `logo` FROM `accounts` WHERE `name`=?"
    			);

				mysqli_stmt_bind_param($stmt, 's', $user_name);
    			mysqli_stmt_execute($stmt);

    			$result = mysqli_stmt_get_result($stmt);
				$logo = "";

				if(mysqli_num_rows($result) > 0) {
					while ($data = mysqli_fetch_assoc($result)) {
						$logo = $data["logo"];
					}
				}

				$img = array(
					'public_id' => $logo,
				);
				echo cl_image_tag(
					$img['public_id'],
					array("format" => "jpg", "width" => "60", "height" => "60", "crop" => "fill")
				);
			?>
			</a>
		</div>

		<br>

		<a class="closebtn" onclick="closeSidebar()">×</a>

		<div style="color: white;" id="username">
			<?php echo $user_name; ?>
		</div>

		<br>
	</div>
	<div class="sidebar-wrapper">
		<ul class="nav" style="color: white;">
			<li>
				<a href="./create-event">
					<i class="far fa-plus-square" title="Create Event"></i>
					<p>CREATE EVENT</p>
				</a>
			</li>
			<li>
				<a href="./my-events">
					<i class="fas fa-stream" title="My Events"></i>
					<p>MY EVENTS</p>
				</a>
			</li>
			<li>
				<a href="./settings">
					<i class="fas fa-cogs" title="Settings"></i>
					<p>SETTINGS</p>
				</a>
			</li>
			<li>
				<a href="./communicate">
     				<i class="fas fa-comments" title="Communication"></i>
      				<p>COMMUNICATION</p>
      			</a>
			</li>
			<li>
				<a href="./bug-report">
     				<i class="fas fa-bug" title="Bug Report"></i>
      				<p>BUG REPORT</p>
      			</a>
			</li>
			<li>
				<a href="./logout">
					<i class="fas fa-sign-out-alt" title="Logout"></i>
					<p>LOGOUT</p>
				</a>
			</li>
		</ul>
		<div class="collapse-btn" style="width: 2rem; height: 2rem;">
			<a href="#"><svg class="tw-icon__svg" width="100%" height="100%" version="1.1" viewBox="0 0 20 20" x="0px" y="0px"><g><path d="M16 16V4h2v12h-2zM6 9l2.501-2.5-1.5-1.5-5 5 5 5 1.5-1.5-2.5-2.5h8V9H6z"></path></g></svg></a>
		</div>
	</div>
</div>
<!-- <script src="../../assets/js/set_src_href.js"></script> -->
