<footer class="footer">
	<text>University Opportunities Ⓒ 2019 • </text>
	<a href="../about-us" id="about-us-a" style="color: #66615B;">About Us</a>
	<text> • </text>
	<i class="fab fa-blogger"></i>
	<a href="https://uni-ties.blogspot.com/" id="blog-link" target="_blank" style="color: #66615B;">Blog</a>
	<text> • </text>
	<a href="../bug-report" id="report-problem-a" style="color: #66615B;">Report a problem</a>
</footer>
