<?php
	session_start();

	include 'includes/functions.php';

	if (!is_user_logged_in()) {
		head("../index.php");
	}

	// Requirements
	include 'config.php';
	require 'configCloud.php';
	require 'settingsCloud.php';

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $user = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        date_default_timezone_set('Europe/Athens');
        $date = date('Y-m-d H:i:s');

        insert_bug_report($user, $email, $subject, $message, $date);
        head("../index.php");
    }


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<title>University Opportunities | Bug Report</title>
</head>
<body>
	<div class="wrapper">
		<?php include 'includes/control-panel.php'; ?>

		<div class="main-panel" id="main-panel">
			<?php include 'includes/nav.php'; ?>

			<div class="content">
				<div class="row">
          <div class="col-md-2" style="">

          </div>
					<div class="col-md-8">
						<div class="card card-user">
							<div class="card-header">
								<h5 class="card-title">Bug Report</h5>
							</div>
							<div class="card-body">
								<form action="bug-report.php" method="POST" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Name</label>
												<input name="name" type="text" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>E-mail</label>
                          						<input name="email" type="email" class="form-control" autocomplete="off" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
										<div class="form-group">
											<label>Subject</label>
											<input name="subject" type="text" class="form-control" maxlength="50" autocomplete="off" required>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
										<div class="form-group">
											<label>Your message</label>
											<textarea name="message" class="form-control" rows="30" maxlength="500" autocomplete="off" required></textarea>
										</div>
										</div>
									</div>
									<button type="submit" onclick="checkFields()" class="btn btn-info" id="settings-button">Send</button>
									<a href="../index.php" class="btn btn-danger" id="cancel-button">Cancel</a>
								</form>
							</div>
						</div>
					</div>
          <div class="col-md-2" style="">

          </div>
				</div>
			</div>
            <?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="../assets/js/bug-report.js"></script>
		<script src="../assets/js/sidebar.js"></script>
</body>
</html>
