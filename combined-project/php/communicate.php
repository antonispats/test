<?php
	session_start();

	include 'includes/functions.php';

	if (!is_user_logged_in()) {
		head("../index.php");
	}

	// Requirements
	include 'config.php';
	require 'configCloud.php';
	require 'settingsCloud.php';

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'includes/head.php'; ?>
	<title>University Opportunities | Communication</title>
</head>
<body>
	<div class="wrapper">
		<?php include 'includes/control-panel.php'; ?>

		<div class="main-panel">
			<?php include 'includes/nav.php'; ?>

			<div class="content">
				<div class="row">
          <div class="col-md-2" style="">

          </div>
					<div class="col-md-8">
						<div class="card card-user">
							<div class="card-header">
								<h5 class="card-title">Communication</h5>
							</div>
							<div class="card-body">
								<form action="create-event.php" method="POST" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>E-mail</label>
                                                <input type="email" class="form-control" autocomplete="off" required>
											</div>
										</div>
									</div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Subject</label>
                        <input type="text" class="form-control" maxlength="30" autocomplete="off" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Your message</label>
                        <textarea name="description"  rows="30" autocomplete="off" class="form-control" required></textarea>
                      </div>
                    </div>
                  </div>
									<button type="submit" class="btn btn-info" id="settings-button">send</button>
									<a href="../index.php" class="btn btn-danger" id="cancel-button">CANCEL</a>
								</form>
							</div>
						</div>
					</div>
          <div class="col-md-2" style="">

          </div>
				</div>
			</div>
            <?php include 'includes/footer.php'; ?>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="../assets/js/liveupdate.js"></script>
	<script src="../assets/js/set_src_href.js"></script>

	<?php include 'includes/scripts.php'; ?>
</body>
</html>
