<?php
session_start();

include 'includes/functions.php';
$conn = start_connection_db();

/**
 * Step 1: User visits this page for the first time
 * So the code runs the ELSE statement because no POST request was made
 * If everything is good -> email is stored in a session variable
 * Step 2: User submits code 
 * So the code runs the IF statement because a POST request was made
 * If the validation is correct -> Go to the reset.php page
 */

// When the user submits the reset password code 
if ($_SERVER['REQUEST_METHOD'] == "POST") {
  // If the cookie has not expired
  if (isset($_COOKIE['temp_access_code'])) {
    if (isset($_POST['code'])) {
      $email = $_SESSION['USER_EMAIL'];
      unset($_SESSION['USER_EMAIL']);
      $validation_code = mysqli_real_escape_string($conn, $_POST['code']);
      // Check if user with this code exist
      $name = validate_user_code($validation_code, $email);
      // If user exist
      if ($name != null) {
        // Set cookie for the reset page | 5 minutes
        setcookie('temp_access_code', $validation_code, time() + 300);
        head("./reset.php?email=$email&code=$validation_code");
      }
    }
  } else {
    // The validation cookies has expired
    $_SESSION['validationCodeMessage'] = "<p class='p-3 mb-2 bg-danger text-white'>Sorry, your validation code has expired</p>";
    head('../index.php');
  }
} else {
  // If the cookie has not expired
  if (isset($_COOKIE['temp_access_code'])) {
    if (!isset($_GET['email']) && !isset($_GET['code'])) {
      head('../index.php');
    } else if (empty($_GET['email']) || empty($_GET['code'])) {
      head('../index.php');
    } else {
      // Get email[From GET]
      $_SESSION['USER_EMAIL'] = mysqli_real_escape_string($conn, $_GET['email']);
    }
  } else {
    // The validation cookies has expired
    $_SESSION['validationCodeMessage'] = "<p class='p-3 mb-2 bg-danger text-white'>Sorry, your validation code has expired</p>";
    head('../index.php');
  }
}  
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <?php include 'includes/head.php'; ?>
  <title>University Opportunities</title>
  <?php include 'includes/head.php'; ?>
</head>
<body>
  <div class="wrapper">
    <div class="main-panel" style="width:100%;">
      <div class="content">
        <div class="row" >
          <div class="col-md-3" style="margin: 0 auto; background-color: white; border-style: solid; border-width: 1px; padding: 15px; margin-top: 5%;">
            <div class="prof_img" style="text-align:center;">
              <img src="../assets/img/em.png" style="width: 25%;">
            </div>
            <div class="prof_img" style="text-align:center; padding: 15px;" id="codePassword-section">
              <p>We have sent you an email with the reset code.</p>
            </div>

            <form action="code.php" method="post">
              <div style="color:red; font-size: 1.2em; font-family: 'Calibri-Light'; text-align: center;" class="form-group">
                <div class="col-auto">
                  <input type="text" class="form-control" name="code" id="code" placeholder="Add your code here" autocomplete="off" required>
                </div>

                <div class="col-auto">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php include 'includes/footer.php'; ?>
    </div>
  </div>

  <?php include 'includes/scripts.php'; ?>
</body>
</html>
