<?php
	session_start();

	// Requirements
	require 'php/configCloud.php';
	require 'php/settingsCloud.php';
	require 'php/config.php';
	include 'php/includes/functions.php';

	/*		Error Handling		*/
	if (isset($_GET['error'])) {
		$errorMessage = $_GET['error'];
		switch ($errorMessage) {
            case "ief":
                $_SESSION['errorMessage'] = "Invalid email format.<br />Try again using a valid email!";
                break;
            case "iep":
                $_SESSION['errorMessage'] = "Invalid email or password!";
                break;
            case "uu":
                $_SESSION['errorMessage'] = "This user is not verified yet!";
                break;
            case "ude":
                $_SESSION['errorMessage'] = "Invalid email or password!";
                break;
            default:
                header("Location: index.php?error");
                exit();
        }
	}
	/*		Error Handling		*/



	$events = array();

	# Fetch filtered events
	if (isset($_GET["category"]) || isset($_GET["type"]) || isset($_GET["month"])) {
		$events = fetch_filtered_events($conn);
	} else if (isset($_GET['q'])) {
        # Fetch events by name
        $search_q = $_GET['q'];
        $events = get_events_by_name($search_q, $conn);
    } else {
        # Fetch all events
		$events = get_all_events($conn, $query = null);
	}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/fav/apple-icon-57x57.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>University Opportunities | Home</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!-- Fonts and icons -->
	<!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"> -->
	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/paper-dashboard.css" rel="stylesheet" />
	<!-- fontawesome -->
	<link rel="stylesheet" href="./assets/icons/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="//wpcc.io/lib/1.0.2/cookieconsent.min.css"/>

</head>
<body>
	<!-- Page Wrapper -->
	<div id="wrapper">


		<?php
		if (is_user_logged_in()) {
			# user is logged in:

			include 'php/includes/control-panel.php';
		} else {
			# user is not logged in:

			include 'php/includes/login-form.php';
		}
		?>

		<div class="main-panel" id="main-panel">

<!--			<div class="pre-navbar">

				<div class="row">

					<a href="https://www.facebook.com/UniversityOpportunities/" id="xshide"><i class="fab fa-facebook-square"></i></a>
					<a href="https://www.instagram.com/uni.ties" id="xshide"><i class="fab fa-instagram"></i></a>
					<a href="#"><i class="fab fa-linkedin"></i></a>
			</div>

		</div>-->

			<?php
				include 'php/includes/nav.php';
			?>
			<div class="content" id="index-page">
				<div class="message">
					<?php
						if (isset($_SESSION['verificationMessage'])) {
							echo $_SESSION['verificationMessage'] . "<br>";
							unset($_SESSION['verificationMessage']);
						} else if (isset($_SESSION['resetMessage'])) {
							echo $_SESSION['resetMessage'] . "<br>";
							unset($_SESSION['resetMessage']);
						} else if (isset($_SESSION['validationCodeMessage'])) {
							echo $_SESSION['validationCodeMessage'] . "<br>";
							unset($_SESSION['validationCodeMessage']);
						} else if (isset($_SESSION['reportBugMessage'])) {
							echo $_SESSION['reportBugMessage'] . "<br>";
							unset($_SESSION['reportBugMessage']);
						} else if (isset($_SESSION['activationLinkSentMessage'])) {
							echo $_SESSION['activationLinkSentMessage'] . "<br>";
							unset($_SESSION['activationLinkSentMessage']);
						}
					?>
				</div>
				<div class="sheader">
					<div class="container">
						<h2>Find everything about seminars
						<br />
						&amp; events
						</h2>
						<br>
						<div class="row justify-content-md-center">
							<div class="col-lg-3 col-md-6 col-sm-6">
								<form id="header-search" action="index.php" method="get">
									<select name="category" class="form-control" id="exampleFormControlSelect1">
										<option value="" disabled selected hidden>- Select Category -</option>
										<option value="Business" name="category[]">Business & Economics</option>
										<option value="Finance" name="category[]">Banking & Finance</option>
										<option value="Marketing" name="category[]">Marketing & HR</option>
										<option value="Management" name="category[]">Management & Logistics</option>
										<option value="IT" name="category[]">IT & Technology</option>
										<option value="Science" name="category[]">Science & Research</option>
										<option value="Medical" name="category[]">Medical & Pharmacy</option>
										<option value="Agriculture" name="category[]">Agriculture & Energy</option>
										<option value="Environment" name="category[]">Enviroment & Waste</option>
										<option value="Travel" name="category[]">Travel & Tourism</option>
										<option value="Erasmus" name="category[]">Erasmus+</option>
									</select>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-6 form-group">
									<input id="search-event-field2" type="text" name="q" class="form-control" placeholder="Search...">
								</div>
								<div class="col-lg-3 col-md-6 col-sm-6">
									<button type="submit" class="btn btn-primary" style="width:100%; margin: 0;" id="cancel-button">Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>

				<header>
					<h3 style="color: #787C90; margin-top: 30px;">Featured Events</h3>
				</header>

				<?php

				$index = 1;
				$last = 0;

				foreach ($events as $event) {
					if ($index - $last == 1) {
						echo "<div class='row'>";
					}
				?>
					<div class="col-lg-3 col-md-6 col-sm-6 event">
						<div class="card" style="width: auto;">

							<!-- Event Creator -->
							<div class="poster" style="padding:15px;">
      							<div class="logo-image-small" id="poster">
      								<!-- insert image here -->
        							<?php
										$event_organizer_info = get_organizer_info_by_event_name($event['name'], $conn);
	        							$img_public_id = $event_organizer_info['logo'];

										echo cl_image_tag(
											$img_public_id,
											array("border-radius" => "50%", "width" => 40, "height" => 40, "gravity" => "face", "radius" => "max", "crop" => "fill")
										);

										echo "&nbsp;&nbsp;&nbsp;";
										$link = "profiles/".$event_organizer_info["id"];
        								echo "<a id='organizer-name' href='$link'>" . htmlspecialchars(ucfirst($event_organizer_info['name']), ENT_QUOTES, 'UTF-8') . "</a>";
        							?>
        						</div>
    						</div>

    						<?php $link = "events/".$event["id"] ?>

							<a href="<?php echo $link ?>">
								<?php
									$img = array(
										'public_id' => $event["image"],
									);
									echo cl_image_tag(
										$img['public_id'],
										array("format" => "jpg", "width" => "448", "height" => "200", "crop" => "scale")
									);
								?>
							</a>
							<div class="card-body">
								<p class="card-title" style="color:red;">
									<?php
									$start_date_formatted = date('d F Y', strtotime($event["start_date"]));
									$start_time_formatted = date('g:i A', strtotime($event["start_time"]));

									echo $start_date_formatted . " at " . $start_time_formatted;
									?>
								</p>
								<h5 class="card-title"><?php echo htmlspecialchars(ucfirst($event["name"])); ?></h5>
								<p class="card-title" style="color:grey;">
                  <?php echo "<i class='fas fa-map-marker-alt'></i>&nbsp;&nbsp;" . htmlspecialchars(ucfirst($event["location"]), ENT_QUOTES, 'UTF-8') ;?>
                </p>
							</div>
						</div>
					</div>

					<?php
					if ($index % 4 == 0) {
						echo "</div>";
						$last = $index;
					}

					$index++;
				}

				//style missing
				if ($index == 1) {
					echo "
					<div style='margin: 25% auto; text-align: center; width: 20%; color: #808080;'>
					<p>No events to show.</p>
					</div>";
				}
				?>
				<div class="row" id="lds-ring">
					<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
				</div>
			</div>
			<?php include 'php/includes/footer.php'; ?>
		</div>
	</div>
		<!--   Core JS Files   -->
		<script src="assets/js/core/jquery.min.js"></script>
		<script src="assets/js/core/popper.min.js"></script>
		<script src="assets/js/core/bootstrap.min.js"></script>
		<!-- Notifications Plugin -->
		<script src="assets/js/plugins/bootstrap-notify.js"></script>
		<script src="assets/js/paper-dashboard.min.js" type="text/javascript"></script>
		<script src="./assets/js/eventsAJAX.js"></script>
		<script src="//wpcc.io/lib/1.0.2/cookieconsent.min.js"></script>
		<script src="./assets/js/cookies.js"></script>
		<script src="assets/js/sidebar.js"></script>
	</body>
</html>
