<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include 'php/includes/head.php'; ?>
	<title>University Opportunities | 404</title>
</head>
<body style="background-color: #EEE;">
	<div class="wrapper">

      <div class="content">
				<div id="main">
		    	<div class="notfound">
		        		<h1>Oops!</h1>
						<h2>404 - Page not found</h2>
						<p>The page you are looking for might have been removed, had its name changed or is temporarily unavailable.</p>
						<a href="../">Go to homepage</a>
		    	</div>
				</div>
      </div>

	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="../assets/js/liveupdate.js"></script>
	<script src="../assets/js/set_src_href.js"></script>
</body>
</html>
