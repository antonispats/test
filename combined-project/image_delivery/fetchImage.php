<?php

// Requirements
require 'config.php';
require 'settings.php';

// Insert Public Id
$publicId = '';

$img = array(
    'public_id' => 'logos/' . $publicId,
    'caption' => 'libedime'
);

echo cl_image_tag(
    $img['public_id'],
    array("format" => "jpg", "crop" => "fill")
);

echo $img['caption'];

?>