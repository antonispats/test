<?php
    // Requirements
    require 'config.php';
    require 'settings.php';
?>

<div id='backend_upload'>
    <h1>Upload through your server</h1>
    <form action="uploadImage.php" method="post" enctype="multipart/form-data">
        <input id="fileupload" type="file" name="image" multiple accept="image/gif, image/jpeg, image/png">
        <input type="submit" value="Upload">
    </form>
</div>



