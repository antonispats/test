<?php

// Requirements
require 'config.php';
require 'settings.php';

function create_photo($file_path, $orig_name)
{
    # Upload the received image file to Cloudinary
    $result = \Cloudinary\Uploader::upload($file_path, array(
            "folder" => "logos/",
            "tags" => "backend_photo_album",
            "public_id" => $orig_name,
            "use_filename" => TRUE
    ));
    unlink($file_path);
    return $result;
}

Class User {
    private $name = 2;

    function __construct( $name, $age ) {
		$this->name = $name;
	}
}

$files = $_FILES["files"];
$files = is_array($files) ? $files : array( $files );
$result = create_photo($files["tmp_name"], $files["name"]);

echo "Public Id :" . $result['public_id'] . '' . "URL" . $result['secure_url'];
?>

<html>
<head>
    <link href="style.css" media="all" rel="stylesheet"/>
    <title>Upload succeeded!</title>
</head>

<?php
/*
# Auto Public Id
$file['unnamed_local'] = \Cloudinary\Uploader::upload($files, $default_upload_options);

# Manual Public Id
$file['unnamed_local'] = \Cloudinary\Uploader::upload('my_image.jpg', array("public_id" => "manutd_id"));

# Preserve Original File Name
$file['unnamed_local'] = \Cloudinary\Uploader::upload('sample.jpg', array("use_filename" => TRUE));

# Dont Know the Format of The Image? 
$file['unnamed_local'] = \Cloudinary\Uploader::upload("spreadsheet.xls", array("resource_type" => "auto"));
*/

# print_r($file);
?>