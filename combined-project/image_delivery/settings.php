<?php
if(file_exists('./env.php')) {
    require './env.php';
}

\Cloudinary::config(array(
    "cloud_name" => getenv('CLOUD_NAME'),
    "api_key" => getenv('API_KEY'),
    "api_secret" => getenv('API_SECRET')
));